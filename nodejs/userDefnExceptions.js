function div(a,b) {
    if (b == 0)
    {
        throw new Error("Denominator is zero in division")
    }
    else
    {
        return a/b;
    }
}

function trycatch()
{
    try
    {
        div(1,0);
    }
    catch (exp)
    {
        console.log(exp)
    }
    finally
    {
        console.log("End of program");
    }
}

trycatch();

