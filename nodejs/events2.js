var events = require('events')

//Event emmiter object
var eventEmitter = new events.EventEmitter();

//generate an event named <periodic> every 2 seconds
var mytimer = setInterval(function(){
    eventEmitter.emit('periodic');
},2000);

//generate an event named <end periodic> every 12 seconds
setTimeout(function(){
    eventEmitter.emit('end_periodic');
},12000);

//create a handler to handle what happens when the event occurs
var periodicHandler = function periodicHandler (){
    console.log('in periodic handler :: interval of 2 seconds');
}

//create a handler to handle what happens when the iend periodic event occurs
var endperiodicHandler = function endperiodicHandler (){
    console.log('in endperiodic handler :: at 12 seconds');
    clearTimeout(mytimer)
}

//connect <periodic> event to the handler called <periodcHandler>
eventEmitter.on('periodic',periodicHandler);
//connect <endperiodic> event to the handler called <endperiodcHandler>
eventEmitter.on('end_periodic',endperiodicHandler);
