"use strict";
var someNumber = 10

// mynNumber will come up as undefined because it is declared (due to hoisting ) but not defined
console.log("some number is : " + someNumber + " my number is: " + myNumber);

// myNumber  will be DECLARED above the previous statement (BUT NOT DEFINED) - it is called hoisting of variable
var myNumber = 100;

// if we use const/let instead of var in above statement we get a "Not defined" error

