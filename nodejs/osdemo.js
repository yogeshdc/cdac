var os = require('os')

console.log('\n*****EOL*****************\n')
console.log(os.EOL);
console.log('\n*********arch()*************\n')
console.log(os.arch());
console.log('\n********constants**************\n')
console.log(os.constants);
console.log('\n******cpus****************\n')
console.log(    os.cpus());
console.log('\n********endianness**************\n')
console.log(os.endianness());
console.log('\n******freemem****************\n')
console.log(os.freemem());
console.log('\n*****homedir*****************\n')
console.log(os.homedir());
console.log('\n******hostname****************\n')
console.log(os.hostname());
console.log('\n********loadavg()**************\n')
console.log(os.loadavg());
console.log('\n*******os.networkInterfaces***************\n')
console.log(os.networkInterfaces());
console.log('\n******platform()****************\n')
console.log(os.platform());
console.log('\n*********release()*************\n')
console.log(os.release());
console.log('\n*********tmpdir()*************\n')
console.log(os.tmpdir());
console.log('\n*****totalmem*****************\n')
console.log(os.totalmem());
console.log('\n*********type*************\n')
console.log(os.type());
console.log('\n********uptime**************\n')
console.log(os.uptime());
console.log('\n**********************\n')

