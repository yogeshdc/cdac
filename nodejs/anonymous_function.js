var x = function(a) {
    console.log(a);
}
x("Hello")

//Normal add func
function add(a,b)
{
    return a+b;
}
console.log(add(5,2));

//Anon add func
//
var x_add = function(a,b) {
    return a+b;
}
console.log(x_add(3,4));

//Anon functions
var anon= function(a) {
    console.log(a);
}
console.log("Anon func: " + anon(8));

//Arrow functions
var anon_arrow=(a)=> {
    console.log(a);
}
console.log("Anon arrow: " + anon_arrow(8));

var x1=()=>{
    console.log("In func x1");
}
x1();

//Arrow for addiion of 3
var add1=(a,b,c) => a+b+c;
console.log("arrow addition of 3 num: " + add1(1,2,3));

//Arrow for isNan
var ifnan=(a) => isNaN(a);
console.log(ifnan(56));
console.log(ifnan("ab"));

//Arrow for multiplication
var mult=(a,b) => a*b;
console.log(mult(10,20));


//Doubling array using anon func
digits=[0,1,2,3,4,5,6,7,8,9];
var root=digits.map(function(temp){
    return temp*2;
});
console.log(root);

//Doubling array using arrow func
var root_arrow=digits.map(a=>a*2);
console.log(root_arrow);

myarr = [0,1,2,3,4,5,6,7,8,9]
var some_operation = myarr.map(a=>(a%2==0)?a+1:a-1)
console.log(some_operation);


//Ways to create object
//1.
var person= {name:"Jon", rno:1, course:"DIOT"}
console.log(person);

//2.
var man = new Object();
man.name = "Doe"
man.rno = 2
man.course = "IoT"
console.log(man);

//3. function and new keyword
function Car(color, name, price) {
    this.color = color;
    this.name = name;
    this.price = price;
}

var car1=new Car('Red','Ford', '550000');
console.log(car1);
//console.log(car1.color)

//printing all attributes
for (var val in car1){
    console.log(val + " : " + car1[val]);

}


function Student (name, class_, roll_no, marks) {
    this.name = name;
    this.class_ = class_;
    this.roll_no = roll_no;
    this.marks = marks;
}

var student1 = new Student ('Diggy', 'DIOT', 11, 98);
var student2 = new Student ('Diggy2', 'DIOT', 12, 99);
var student3 = new Student ('Diggy3', 'DIOT', 13, 97);
var student4 = new Student ('Diggy4', 'DIOT', 14, 96);

var stdarr = [student1, student2, student3, student4]

for (var i=0; i<stdarr.length; i++)
{
    console.log("Student name is " + stdarr[i].name +  " of class " + stdarr[i].class_ + " whose roll number " + stdarr[i].roll_no + " and marks obtained is "+ stdarr[i].marks);
}

//[array of objects]
//book - name, author, reading_status(bool)
//book1 {name:C in depth, author, k srivastav, reading_status: false}
// if reading_status = false => This bookl has been read, else this book has been completed
function Book (name, author, reading_status) {
    this.name = name;
    this.author = author;
    this.reading_status = reading_status;
}
var book1 = new Book ( "C in Depth", "K Srivastav", false)
var book2 = new Book ( "Let us C", "Yashavant-Kanetkar", false)
var book3 = new Book ( "Fundamentals IoT", "Gonzales", true)
var book4 = new Book ( "Linux System Programming", "Robert Love", true)
book_arr = [book1, book2, book3, book4]
/*for ( var i = 0; i < book_arr.length; i++) {
    if (book_arr[i].reading_status == true)    {
        console.log (book_arr[i].name + " has been completed.")
    }
    else {
        console.log (book_arr[i].name + " has been to be read.")
    }
}
*/
for ( var i in book_arr) {
    if (book_arr[i].reading_status == true)    {
        console.log (book_arr[i].name + " has been completed.")
    }
    else {
        console.log (book_arr[i].name + " has been to be read.")
    }
}
