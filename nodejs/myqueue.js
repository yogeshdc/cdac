function stack() {
    this .arr = [];
    this.top = -1;
}

stack.prototype.spush = function (x) {
    this.arr[++this.top]=x;
}
stack.prototype.spop = function () {
    var tmp = this.arr.shift();
    if (tmp) {
    this.top--;
    }
    return tmp;
}

var s1=new stack();
for (var i=1; i<=10; i++)
{
    s1.spush(i*10);
    console.log(s1)
}
for (var i=1; i<=10; i++)
{
    s1.spop(i*10);
    console.log(s1)
}
