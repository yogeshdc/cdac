var w = {
    temp    :   24,
    hum     :   20,
    area    :   ['Aundh', 'Pashan', 'Baner'],
    get_temp:   function(){
        console.log('The temperature is: ' + this.temp);
        return this.temp;
    }
}
console.log(w);
console.log(w.area);
w.get_temp();
console.log(w.get_temp());
console.log('\n\n\n****************************\n\n\n');

function weather (wtemp) {
    console.log(wtemp.temp);
    console.log(wtemp.hum);
    console.log(wtemp.area);
}
weather(w);
console.log('\n\n\n****************************\n\n\n');

function box(color) {
    var tbox = {
        height  :   20,
        width   :   10,
        boxcolor:   color
    }
    return(tbox);
}

var b1 = box("Yellow");
console.log(b1);

console.log('\n\n\n**********Calculator functions******************\n\n\n');

var calculator = {
    //1st add is the variable of object calculator.
    //This is the add that gets called when we say calculator.add
    //
    //The 2nd add is the arrow function
    //This is the function that returns the value when 1st add is called
    add: add=(a,b)=>a+b,
    sub: sub=(a,b)=>a-b,
    div: div=(a,b)=>a/b,
    mul: mul=(a,b)=>a*b
}

var addition = calculator.add(20,10);
var subtract= calculator.sub(20,10);
var divide = calculator.div(20,10);
var multiply = calculator.mul(20,10);
console.log(addition);
console.log(subtract);
console.log(divide);
console.log(multiply);
console.log('\n\n\n****************************\n\n\n');



console.log('\n\n\n********object to array********************\n\n\n');


function object_to_array()
{
    var myobj = {
        'red'     :   1,
        'blue'    :   12,
        'yellow'  :   31
    }
    myarr=[]
    for (var val in myobj)
    {
        elem = [val,myobj[val]];
        myarr.push([elem]);
    }
    console.log(myarr);
    return myarr;
}
object_to_array();

console.log('\n\n\n********object to object_to_inverted_object********************\n\n\n');
function object_to_inverted_object()
{
    var myobj = {
        'red'     :   1,
        'blue'    :   12,
        'yellow'  :   31
    }
    myarr2=[]
    for (var val in myobj)
    {
        elem = [myobj[val], val];
        var mynewobj = {key: elem[0], value: elem[1]};
        myarr2.push(mynewobj);
    }
    console.log(myarr2);
    return myarr2;
}
object_to_inverted_object();

console.log('\n\n\n********my object********************\n\n\n');

//This will export all objects from this file when any other file user require("object_demo")
//
module.exports = {};
module.exports.w = w;
module.exports.wcalc = calculator;
module.exports.wfunc = object_to_inverted_object;
