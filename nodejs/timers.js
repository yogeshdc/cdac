

//sync call
//setInterval (function, timeout in ms)
var t3 = setInterval(function(){
    console.log('In interval functioni with delay: 2 seconds');
},2000);


//async call
//setTimeOut
//
setTimeout(function(){
    console.log('In setTimeout')
    clearTimeout(t3)
},6010);
