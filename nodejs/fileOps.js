var fs = require('fs')
var buf = new Buffer(1024);

fs.open('input.txt','r+',function(err,fd) {
    if (err) {
        return console.log(err);
    } else {
        console.log('File opened successfully');
        console.log('Beginning Read...');
        //Args of fs.read : (file descripter, buffer name, offset to start reading,  buffer length, position, function)
        fs.read(fd, buf, 0, buf.length, 0, function(err,data) {
            if (err) {
                return console.log(err);
            } else {
                console.log(buf.slice(0,data).toString());
            }
        });
        /*fs.write(fd, "This is also another hello world ", 0, function (err) {
            if (err) {
                return console.log(err);
            } else {
                console.log("File written successfully")
            }
        })*/
        /*fs.read(fd, buf, 0, buf.length, 0, function(err,data) {

            if (err) {
                return console.log(err);
            } else {
                return console.log(buf.slice(0,data).toString());
            }
        });*/

    }
});

/*
fs.writeFile('input.txt', 'Hello World!!!', function(err) {
    if (err) {
        return console.log(err);
    } else {
        console.log('File written successfully');
        fs.readFile('input.txt', function(err, data) {
            if (err) {
                return console.log(err);
            } else {
                return console.log(data.toString());
            }
        })
    }
})

*/
