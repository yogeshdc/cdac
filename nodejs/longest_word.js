function longest_word()
{
    var myString = "This is a random sentence"
    var strarr = myString.split(' ')
    var i = 0
    var maxlen = 0;
    var maxstring = '';
    while (strarr[i])
    {
        if (maxlen < strarr[i].length)
        {   
            maxlen = strarr[i].length;
            maxstring = strarr[i];
        }
        i++;
    }
    console.log("Longest word in " + myString + " is: " + maxstring)
}

function orderedString()
{
    var string = "welcometoIOT";
    console.log ( string + " in sorted format is " + string.split('').sort().join('') )
}

function stringCombinations()
{
    var myStr = "Hello"
    strLen = myStr.length
    console.log("Combinations of " + myStr + " are: ")
    for (var i=0; i<strLen; i++)
    {
        for (var j=i+1; j<strLen+1; j++)
            {
                console.log( myStr.substring(i,j))
            }
    }
}

longest_word();
orderedString();
stringCombinations();
