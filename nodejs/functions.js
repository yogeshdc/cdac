function add(a,b)
{
    return (a+b)
}

function palindrome(str)
{
    var flag = true;
    for (var i=0; i<str.length/2; i++)
    {
        if (str[i] == str[str.length-i-1])
            continue;
        else
        {
            flag = false;
            break
        }

    }
    return flag;
}

function factorial(num)
{
    var result = 1;
    while (num > 1)
    {
        result = result*num;
        num--;
    }
    return result;
}

var sum = add(2,2);
console.log(sum);
var ispalin = palindrome("Hello");
console.log(ispalin);
ispalin = palindrome("malayalam");
console.log(ispalin);
var fact = factorial(5);
console.log(fact);
