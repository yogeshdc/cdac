function stack() {
    this .arr = [];
    this.top = -1;

}

stack.prototype.spush = function (x) {
    this.arr[++this.top]=x;
}
stack.prototype.spop = function () {
    //var tmp = this.arr[this.top--];
    var tmp = this.arr.pop();
    this.top--;
    return tmp;
}

var s1=new stack();
/* 
 * s1.spush(10);
s1.spush(20);
s1.spush(30);
s1.spush(40);
s1.spush(50);
console.log(s1);
s1.spop();
s1.spop();
s1.spop();
s1.spop();
s1.spop();

console.log(s1);
*/
for (var i=1; i<=10; i++)
{
    s1.spush(i*10);
    console.log(s1)
}
for (var i=1; i<=10; i++)
{
    s1.spop(i*10);
    console.log(s1)
}
