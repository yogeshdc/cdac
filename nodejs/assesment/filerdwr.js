var readline = require('readline');

function write_read_file () {
    var filename = '';
    var rl = readline.createInterface({
        input   :   process.stdin,
        output  :   process.stdout
    });

    rl.on('line',(input)=>{
        filename = input;
        rl.close();
    })

    var fs = require('fs')
    var buf = new Buffer(1024);
    setTimeout(function(){
        fs.open(filename,'r+',function(err,fd) {
            if (err) {
                return console.log(err);
            } else {
                console.log('File opened successfully');
                console.log('Beginning Write...');
                fs.write(fd, "This is a random string entered into file ", 0, function (err) {
                    if (err) {
                        return console.log(err);
                    } else {
                        console.log("File written successfully");
                    }
                    //Args of fs.read : (file descripter, buffer name, offset to start reading,  buffer length, position, function)
                    fs.read(fd, buf, 0, buf.length, 0, function(err,data) {
                        if (err) {
                            return console.log(err);
                        } else {
                            console.log('Beginning Read...');
                            return console.log(buf.slice(0,data).toString());
                        }
                    });
                })
            }
        });
    }, 5000);
}

write_read_file();
