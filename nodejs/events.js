var events = require('events')

//Event emmiter object
var eventEmitter = new events.EventEmitter();

//generate an event named <periodic> every 2 seconds
var mytimer = setInterval(function(){
    eventEmitter.emit('periodic');
},2000);

//create a handler to handle what happens when the event occurs
var periodicHandler = function periodicHandler (){
    console.log('in periodic handler :: interval of 2 seconds');
}

//connect <periodic> event to the handler called <periodcHandler>
eventEmitter.on('periodic',periodicHandler);
