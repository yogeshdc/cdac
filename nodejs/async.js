var fs = require ('fs');

fs.readFile('input.txt', function (err, data) {
    if (err) {
        console.log(err);
    }
    else {
        console.log('Async :: '+ data.toString());
    }
});


//The above call is async. So if it blocks for getting data from the file
//then the code execution will continue from the below log command.
//So we "may" see the "program end" in output <before> input.txt file data
console.log('program end');
