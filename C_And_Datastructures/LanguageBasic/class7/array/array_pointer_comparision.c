/*
 * =====================================================================================
 *
 *       Filename:  array_pointer_comparision.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Monday 28 August 2017 08:39:27  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include	<stdlib.h>
#include	<stdio.h>
void display_line ();
/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
int main ( int argc, char *argv[] )
{
    const int size = 10;
    int arr[size];

    display_line();

    //Initilizing array from 0 to 9
    for ( int i=0; i<size; i++ )
    {
        arr[i] = i;
    }

    //Printing after initilizing
    printf ( "\n\nAfter Init\n" );
    for ( int i=0; i<size; i++ )
    {
        printf ( "arr[%d] = %d\n",i,arr[i] );
    }
    display_line();

    //Prining addresses of all array elements
    printf ( "\n\nPrint Address of array elements\n" );
    for ( int i=0; i<size; i++ )
    {
        printf ( "Address of arr[%d] = %p\n",i,&arr[i] );
    }
    display_line();


    //Printing array elements using pointer notation
    printf ( "\n\nArray printing using pointer notation\n" );
    for ( int i=0; i<size; i++ )
    {
        printf ( "*(arr+%d) = %d\n",i,*(arr+i) );
    }
    display_line();

    //Printing address of array elements using pointer notation
    printf ( "\n\nArray address printing using pointer notation\n" );
    for ( int i=0; i<size; i++ )
    {
        printf ( "(arr+%d) = %p\n",i,(arr+i) );
    }
    display_line();

    printf ( "%p\n",&arr[0] );
    printf ( "%p\n",&arr[0]+1 );
    printf ( "%p\n",&arr[10] );
    printf ( "%p\n",&arr );
    printf ( "%p\n",arr+1 );



    //Printing element and address in one line
    printf ( "\n\nAll in One\n" );
    for ( int i=0; i<size; i++ )
    {
        printf ( "arr[%d] = %d \t *(arr+%d) = %d \t\t|\t &arr[%d] = %p \t (arr+%d) = %p\n",i,arr[i], i,*(arr+i), i,&arr[i], i,(arr+i) );
    }
    display_line();

    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */



/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  display_line
 *  Description:  
 * =====================================================================================
 */
    void
display_line ( )
{
    for (int i=0;i<80;i++)
        printf("*");
    printf("\n");
}		/* -----  end of function display_line  ----- */
