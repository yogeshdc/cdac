/*
 * =====================================================================================
 *
 *       Filename:  second_highest.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Monday 28 August 2017 08:20:59  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  second_highest
 *  Description:  
 * =====================================================================================
 */

int second_highest ( int *arr, int size )
{
    int max = *(arr+0);
    int counter = 0;
    for (int i=1; i<size; i++)
    {
        if (max<*(arr+i) )
        {
            max = *(arr+i);
            counter = i;
        }
    }

    int second_max = *(arr+0);
    int second_counter = 0;
    for (int i=1;i<size;i++)
    {
        if (second_max<*(arr+i) && counter!=i)
        {
            second_max = *(arr+i);
            second_counter = i;
        }
    }

    return second_counter;


}		/* -----  end of function second_highest  ----- */
