/*
 * =====================================================================================
 *
 *       Filename:  sort_array.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Monday 28 August 2017 08:01:16  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  sort_array
 *  Description:  
 * =====================================================================================
 */
    void
sort_array ( int* arr, int size )
{
    
    for ( int i=0; i<size ;i++ )
    {
        for ( int j=i+1; j<size; j++)
        {
            if ( *(arr+i) > *(arr+j) )
            {
                *(arr+i) = *(arr+i) + *(arr+j);
                *(arr+j) = *(arr+i) - *(arr+j);
                *(arr+i) = *(arr+i) - *(arr+j);
            }
        }
    }
}		/* -----  end of function sort_array  ----- */
