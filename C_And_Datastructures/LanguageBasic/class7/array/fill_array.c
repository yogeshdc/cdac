/*
 * =====================================================================================
 *
 *       Filename:  fill_array.c
 *
 *    Description
 *
 *        Version:  1.0
 *        Created:  Monday 28 August 2017 07:14:49  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */
#include <stdio.h>


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  fill_array
 *  Description:  
 * =====================================================================================
 */
    void
fill_array ( int* arr, int size )
{
    printf ( "size of arr = %lu\n",sizeof(arr) );
    for ( int i=0; i<size ;i++ )
    {
        *(arr+i) = i;
    }

}		/* -----  end of function fill_array  ----- */
