/*
 * =====================================================================================
 *
 *       Filename:  pass_array_by_address.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Monday 28 August 2017 07:04:29  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */

//COMPILE USING: gcc pass_array_by_address.c fill_array.c display_array.c exchange_elements.c sort_array.c second_highest.c second_lowest.c 
//

#include	<stdlib.h>
#include	<stdio.h>
#include        "pass_array_by_address.h"
/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
    int
main ( int argc, char *argv[] )
{
    const int size = 10;
    int arr[size];
    char ch;
    
    
    do {
    //Fill array by function that passes by address
        fill_array(arr, size);
        printf ( "Sizeof arr = %lu\n",sizeof(arr) );
    

        for ( int i=0; i<size ;i++ )
        {
            //Display array by function that passes by value
            display_array(arr[i]);
        }


        
        exchange_elements(arr, size);

        for ( int i=0; i<size ;i++ )
        {
            //Display array by function that passes by value
            display_array(arr[i]);
        }



        sort_array (arr, size);
        for ( int i=0; i<size ;i++ )
        {
            //Display array by function that passes by value
            display_array(arr[i]);
        }


        int index = second_highest(arr, size);
        printf ( "Second highest element is %d at index %d\n",arr[index],index );

        index = second_lowest(arr, size);
        printf ( "Second lowest element is %d at index %d\n",arr[index],index );



        printf ( "Do you want to continue?\n" );
        scanf ( "%c", &ch );
    } while ( ch == 'y' || ch == 'Y' );				/* -----  end do-while  ----- */

    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */

