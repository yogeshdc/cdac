/*
 * =====================================================================================
 *
 *       Filename:  pass_array_by_address.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Monday 28 August 2017 07:15:44  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */

//Pointer Notation
void fill_array (int*, int);

//Pass by Value Notation
void display_array (int);


void exchange_elements (int*, int);
//Array Notation
//void fill_array(int arr[10]);
//void displaty (int arr[10]);
//
void sort_array ( int* , int ); 
int second_highest ( int *, int );
int second_lowest ( int *, int );
