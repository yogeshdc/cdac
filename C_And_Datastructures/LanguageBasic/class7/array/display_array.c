/*
 * =====================================================================================
 *
 *       Filename:  display_array.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Monday 28 August 2017 07:19:21  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */
#include <stdio.h>

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  display_array
 *  Description:  
 * =====================================================================================
 */
    void
display_array ( int num )
{

    printf ( "%d\n", num);
}		/* -----  end of function display_array  ----- */
