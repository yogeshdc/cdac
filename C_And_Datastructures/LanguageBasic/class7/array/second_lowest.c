/*
 * =====================================================================================
 *
 *       Filename:  second_highest.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Monday 28 August 2017 08:20:59  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */
#include <stdlib.h>
#include <limits.h>

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  second_lowest
 *  Description:  
 * =====================================================================================
 */

int second_lowest ( int *arr, int size )
{
    int min = *(arr+0);
    int counter = -1;
    for (int i=1; i<size; i++)
    {
        if (min>*(arr+i) )
        {
            min = *(arr+i);
            counter = i;
        }
    }

    int second_min = 999999;//*(arr+0);
    int second_counter = -1;
    for (int i=1;i<size;i++)
    {
        if (second_min>*(arr+i) && counter!=i)
        {
            second_min = *(arr+i);
            second_counter = i;
        }
    }

    return second_counter;


}		/* -----  end of function second_lowest  ----- */
