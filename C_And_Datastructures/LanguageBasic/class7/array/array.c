/*
 * =====================================================================================
 *
 *       Filename:  array.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Monday 28 August 2017 08:08:34  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include	<stdlib.h>
#include	<stdio.h>

void display_line();
/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
    int
main ( int argc, char *argv[] )
{
    const int size = 10;
    int arr[size];

    printf ( "Sizeof array = %lu\n",sizeof(arr) );
    printf ( "Num of elements in array = %lu\n",sizeof(arr)/sizeof(int) );
    display_line();


    //Will print random garbage without initilizing
    printf ( "Without Init\n" );
    for ( int i=0; i<size; i++ )
    {
        printf ( "arr[%d] = %d\n",i,arr[i] );
    }
    display_line();

    //Initilizing array from 0 to 9
    for ( int i=0; i<size; i++ )
    {
        arr[i] = i;
    }

    //Printing after initilizing
    printf ( "\n\nAfter Init\n" );
    for ( int i=0; i<size; i++ )
    {
        printf ( "arr[%d] = %d\n",i,arr[i] );
    }
    display_line();


    //Partial Init
    //Note: Partial init requires int value for size. It will NOT accept const int variable
    int arr2[10] = {0};
    //Printing after initilizing
    printf ( "\n\nAfter partial Init\n" );
    for ( int i=0; i<size; i++ )
    {
        printf ( "arr2[%d] = %d\n",i,arr2[i] );
    }
    display_line();


    printf ( "\n\nPrint Address of array elements\n" );
    for ( int i=0; i<size; i++ )
    {
        printf ( "arr2[%d] = %p\n",i,&arr2[i] );
    }
    display_line();


    int arr3[size];
    //Read value from console using scanf
    //Initilizing array elements from 0 to 9
    printf ( "Enter array values: \n" );
    for ( int i=0; i<size; i++ )
    {
        printf ( "Enter a number: \n" );
        scanf ( "%d", &arr3[i] );
    }
    display_line();



    printf ( "\n\nAfter user input\n" );
    for ( int i=0; i<size; i++ )
    {
        printf ( "arr3[%d] = %d\n",i,arr3[i] );
    }
    display_line();


    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */



/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  display_line
 *  Description:  
 * =====================================================================================
 */
    void
display_line ( )
{
    for (int i=0;i<80;i++)
        printf("*");
    printf("\n");
}		/* -----  end of function display_line  ----- */
