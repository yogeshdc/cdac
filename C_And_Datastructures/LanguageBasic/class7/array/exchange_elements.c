/*
 * =====================================================================================
 *
 *       Filename:  exchange_elements.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Monday 28 August 2017 07:42:13  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */
#include <stdio.h>

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  exchange_elements
 *  Description:  
 * =====================================================================================
 */
    void
exchange_elements ( int* arr, int size )
{
    if (size%2!=0)
        size-=1;
    for (int i=0; i<size; i+=2)
    {
        *(arr+i) = *(arr+i) + *(arr+i+1);
        *(arr + i + 1) = *(arr+i) - *(arr+i+1);
        *(arr+i) = *(arr+i) - *(arr+i+1);
    }

    
    for ( int i=0; i<size ;i++ )
    {
        printf ( "---------------------------------\n" );
        printf ( "%d\t",*(arr+i) );
        printf ( "---------------------------------\n" );
    }
}		/* -----  end of function exchange_elements  ----- */
