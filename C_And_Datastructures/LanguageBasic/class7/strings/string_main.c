/*
 * =====================================================================================
 *
 *       Filename:  string_main.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Monday 28 August 2017 08:54:31  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include	<stdlib.h>
#include        <stdio.h>
#include        "mystring.h"

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
    int
main ( int argc, char *argv[] )
{
    char str[] = "DIOT";
    char str2[] = "PG_DIOT";
    char str3[50] = "";




    int len = my_strlen(str);
    printf ( "Size of str is %d\n", len );
    
    int comparision = my_strcmp (str, str2);
    printf ( "Comparision of str=%s and str2=%s = %d\n", str, str2, comparision );

    my_strcpy (str3, str2);
    printf ( "%s\n",str3 );

    my_strrev (str);
    printf ( "Reversed String is: %s \n", str );
    
    //my_strconcat (str, str2);
    //printf ( "Concatenated string is: %s\n", str );
    
    
    
    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */
