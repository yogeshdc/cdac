/*
 * =====================================================================================
 *
 *       Filename:  my_strlen.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Monday 28 August 2017 08:56:13  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  my_strlen
 *  Description:  
 * =====================================================================================
 */
int my_strlen ( char* str )
{
    int i = 0;
    while (str[i] != '\0')
    {
        i++;
    }
    return i;

}		/* -----  end of function my_strlen  ----- */
