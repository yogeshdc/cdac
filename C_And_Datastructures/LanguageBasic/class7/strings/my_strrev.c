/*
 * =====================================================================================
 *
 *       Filename:  my_strrev.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Tuesday 29 August 2017 07:37:08  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */
#include "mystring.h"
#include <stdio.h>



/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  my_strrev
 *  Description:  
 * =====================================================================================
 */
    void
my_strrev ( char * mystr )
{
    int len = my_strlen (mystr);
    int i = 0;
    static char rev[50];
    while (i<len)
    {
        rev[i] = mystr[len-i-1];
        i++;
    }
    rev[i] = '\0';
    //printf ( "Rev = %s\n",rev );
    my_strcpy(mystr,rev);
    //printf ( "string = %s\n",mystr );
}		/* -----  end of function my_strrev  ----- */
