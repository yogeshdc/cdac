/*
 * =====================================================================================
 *
 *       Filename:  my_strcmp.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Monday 28 August 2017 08:58:50  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */

#include <stdio.h>
/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  my_strcmp
 *  Description:  
 * =====================================================================================
 */
int my_strcmp ( char* str1, char* str2 )
{
    int i = 0;
    int j = 0;
    while ( str1[i] != '\0')
    {
        if (str2[i] == '\0')
        {
            break;
        }
            j = str1[i] - str2[i];
            if (j != 0)
            {
                break;
            }
            else
                i++;

    //printf ( "%d \t %c \t %c \n",i, str1[i],str2[i] );
    }
    return j;
}		/* -----  end of function my_strcmp  ----- */
