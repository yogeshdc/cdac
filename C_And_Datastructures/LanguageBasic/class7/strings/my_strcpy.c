/*
 * =====================================================================================
 *
 *       Filename:  my_strcpy.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Monday 28 August 2017 09:27:09  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  my_strcpy
 *  Description:  
 * =====================================================================================
 */
void my_strcpy ( char* str_to, char* str_from )
{
    int i = 0;
    while (str_from[i] != '\0')
    {
        str_to[i] = str_from[i];
        i++;
    }
}		/* -----  end of function my_strcpy  ----- */
