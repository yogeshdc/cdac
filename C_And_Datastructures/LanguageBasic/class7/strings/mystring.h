/*
 * =====================================================================================
 *
 *       Filename:  mystring.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Monday 28 August 2017 08:52:46  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


int my_strlen (char*);

int my_strcmp (char*, char*);

void my_strcpy (char*, char*);

void my_strrev (char*);

void my_strconcat (char*, char*);
