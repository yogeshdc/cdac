/*
 * =====================================================================================
 *
 *       Filename:  ex14Cf.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Monday 04 September 2017 01:56:07  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include	<stdlib.h>
#include	<stdio.h>

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
    int
main ( int argc, char *argv[] )
{
    int x[3][5] = {
        {1,2,3,4,5},
        {6,7,8,9,10},
        {11,12,13,14,15}
    };
    int *n = &x;

    printf ( "%d\n", *(*(x+2)+1) );
    printf ( "%d\n", *(*x+2)+5 );
    printf ( "%d\n", *(*(x+1)) );
    printf ( "%d\n", *(*(x)+2)+1 );
    printf ( "%d\n", *(*(x+1)+3) );
    printf ( "%d\n", *n );
    printf ( "%d\n", *(n+2) );
    printf ( "%d\n", *(n+3)+1 );
    printf ( "%d\n", *(n+5)+1 );
    printf ( "%d\n", ++*n );

    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */
