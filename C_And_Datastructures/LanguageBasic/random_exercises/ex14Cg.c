/*
 * =====================================================================================
 *
 *       Filename:  ex14Cg.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Monday 04 September 2017 02:17:52  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include	<stdlib.h>
#include	<stdio.h>

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
    int
main ( int argc, char *argv[] )
{
    int arr[3][3] = {
        2,4,6,
        9,1,10,
        16,64,5
    };

    printf ( "%d\n",**arr );
    printf ( "%d\n",**arr < *(*arr+3) );
    //printf ( "%d\n",*(arr+2)/( *(*arr+1) > **arr) );
    printf ( "%d\n",*(arr[1]+1) | arr[1][2] );
    printf ( "%d\n",*(arr[0]) | *(arr[2]) );
    printf ( "%d\n",arr[1][1] < arr[0][1] );
    printf ( "%d\n",arr[2][1] & arr[2][0] );
    printf ( "%d\n",arr[2][2] | arr[0][1] );
    printf ( "%d\n",arr[0][1] ^ arr[0][2] );
    printf ( "%d\n",++**arr + --arr[1][1] );
    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */
