/*
 * =====================================================================================
 *
 *       Filename:  b.c
 *
 *    Description:  (b) Any integer is input through the keyboard. Write a program to
 *    find out whether it is an odd number or even number.
 *
 *        Version:  1.0
 *        Created:  Tuesday 22 August 2017 07:26:13  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include	<stdlib.h>

#include	<stdio.h>

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
int main ( int argc, char *argv[] )
{
	int a;
	printf("Enter a no:");
	scanf("%d",&a);
	if(a%2 ==0)
		printf("Even\n");
	else
		printf("Odd\n");

	return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */
