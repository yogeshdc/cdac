/*
 * =====================================================================================
 *
 *       Filename:  leapyear.c
 *
 *    Description:  Leapyear Calculator
 *
 *        Version:  1.0
 *        Created:  Tuesday 22 August 2017 06:54:42  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include	<stdlib.h>
#include	<stdio.h>

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
int main ( int argc, char *argv[] )
{
	int year, leap = 0;
	printf ("Enter a year: ");
	scanf ("%d",&year);
	if (year%4 == 0) 
	{
		if (year%100 == 0)
		{
			if (year%400 == 0)
			leap = 1;
		
			else
				leap = 0;
		}
		else
		{
			leap = 1;
		}
	}
	
	if (leap == 1) 
		printf ("Its a leap year!!!\n");
	else
		printf ("Its NOT a leap year!!!\n");


	return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */
