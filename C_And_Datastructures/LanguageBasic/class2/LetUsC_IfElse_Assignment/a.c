/*
 * =====================================================================================
 *
 *       Filename:  a.c
 *
 *    Description:  (a) If cost price and selling price of an item is input through the
 *    keyboard, write a program to determine whether the seller has
 *    made profit or incurred loss. Also determine how much profit
 *    he made or loss he incurred.
 *
 *        Version:  1.0
 *        Created:  Tuesday 22 August 2017 07:20:27  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include	<stdio.h>
#include	<stdlib.h>

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
int main ( int argc, char *argv[] )
{
	int cost_price, selling_price, profit_loss;
	printf ("Enter the cost price of the item:");
	scanf ("%d",&cost_price);
	printf ("Enter the iselling price of the item:");
	scanf ("%d",&selling_price);
	profit_loss = selling_price - cost_price;
	if (profit_loss >=0 )
		printf ("Seller made a profit of %d\n", profit_loss);
	else
		printf ("Seller incured a loss of %d\n", profit_loss);
	
	return EXIT_SUCCESS;
}
				/* ----------  end of function main  ---------- */
