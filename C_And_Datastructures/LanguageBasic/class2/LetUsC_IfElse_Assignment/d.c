/*
 * =====================================================================================
 *
 *       Filename:  d.c
 *
 *    Description:  According to the Gregorian calendar, it was Monday on the
 *    date 01/01/1900. If any year is input through the keyboard
 *    write a program to find out what is the day on 1 st January of
 *    this year
 *
 *        Version:  1.0
 *        Created:  Tuesday 22 August 2017 07:34:32  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include	<stdlib.h>
#include	<stdio.h>
#include	<math.h>
int calculateDayOfYear ( int );


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description: To calculate we use the formula of Disparate variation
 *  Refer https://en.wikipedia.org/wiki/Determination_of_the_day_of_the_week
 *
 * =====================================================================================
 */
int main ( int argc, char *argv[] )
{
	int year;
	printf ("Enter a year: ");
	scanf ("%d",&year);

	int w = calculateDayOfYear (year);
	printf("1st Jan of %d was %d",year,w);
	return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */



/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  calculateDayOfYear
 *  Description:  
 *
 *  w = (d + (2.6m - 0.2)floor +y + (y/4)floor + (c/4)floor -2c ) mod 7
 *  where,
 *
 *   Y is the year minus 1 for January or February, and the year for any other month
 *   y is the last 2 digits of Y
 *   c is the first 2 digits of Y
 *   d is the day of the month (1 to 31)
 *   m is the shifted month (March=1,...,February=12)
 *   w is the day of week (0=Sunday,...,6=Saturday). If w is negative you have to add 7 to it.
 * =====================================================================================
 */
	
int calculateDayOfYear ( int year )
{
	int y = (year-1)%100;
	int c = (year-1)/100;
	int w = ((int)(1 + floor(2.6*11-0.2) + y + floor(y/4.0) + floor(c/4.0) - 2*c )) % 7;
	if (w<0)
		w = 7+w;
	return w;
}		/* -----  end of function calculateDayOfYear  ----- */
