/*
 * =====================================================================================
 *
 *       Filename:  randomExample.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Tuesday 22 August 2017 07:46:38  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */

#include <stdio.h>

int main()
{
	float a = -48.9;
	int b;
	b = a;
	printf("%d",b);
	return 0;
}
