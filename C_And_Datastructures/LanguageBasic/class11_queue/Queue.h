/*
 * =====================================================================================
 *
 *       Filename:  queue.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Monday 04 September 2017 07:42:19  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */

#pragma oncie
#define         SIZE    5
#include        <stdbool.h>
#include        <stdio.h>

struct Queue
{
    int arr[SIZE];
    int front;
    int rear;
};

bool IsFull (struct Queue*);
bool IsEmpty (struct Queue*);
void Push (int, struct Queue*);
int Pop (struct Queue*);
void Peek(struct Queue*);
