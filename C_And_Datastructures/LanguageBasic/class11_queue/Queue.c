/*
 * =====================================================================================
 *
 *       Filename:  Stack.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Monday 04 September 2017 07:47:37  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */
#include        "Stack.h"
#include        <stdbool.h>
#include        <stdio.h>

bool IsFull (struct Stack* s)
{
    printf ( "Top is at: %d\n", s->top );
    if(s->top >= SIZE)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool IsEmpty (struct Stack* s)
{
    printf ( "Top is at: %d\n", s->top );
    if(s->top <= 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}

void Push (int element, struct Stack* s)
{
    printf ( "Top is at: %d\n", s->top );
    if(!IsFull(s))
    {
        s->arr[s->top] = element;
        s->top++;
        printf ( "Pushed %d to top of stack.\n",element );
    }
    else
    {
        printf ( "Stack is already full, Cannot push %d to stack\n",element );
    }
}

int Pop (struct Stack* s)
{
    printf ( "Top is at: %d\n", s->top );
    if(!IsEmpty(s))
    {
        s->top--;
        return s->arr[s->top];
    }
    else
    {
        printf ( "Stack is empty, Cannot pop anything\n" );
    }
}

void Peek(struct Stack* s)
{
    printf ( "Top is at: %d\n", s->top );
    if (IsEmpty(s))
    {
        printf ( "Stack is empty, Cannot show anything\n" );
    }
    else
    {
        printf ( "Top element is: %d\n", s->arr[s->top] );
    }
}
