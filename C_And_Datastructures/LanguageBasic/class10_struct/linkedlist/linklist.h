/*
 * =====================================================================================
 *
 *       Filename:  linklist.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Saturday 02 September 2017 02:24:18  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */
#pragma once

struct linklist
{
    int data;
    struct linklist *next;
};

struct linklist* create_newnode ();
void display_linklist (struct linklist* );
void add_node_at_end (struct linklist * );
void add_node_in_middle (struct linklist * );
struct linklist* add_node_at_beginning (struct linklist * );
struct linklist* delete_node_at_beginning (struct linklist *);
struct linklist* reverse_linklist (struct linklist *);
void delete_node_at_end (struct linklist *);
void delete_node_in_middle ( struct linklist* );
