/*
 * =====================================================================================
 *
 *       Filename:  delete_node_at_beginning.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Saturday 02 September 2017 05:01:24  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */

#include        <stdio.h>
#include        <stdlib.h>
#include        "linklist.h"

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  delete_node_at_beginning
 *  Description:  
 * =====================================================================================
 */
struct linklist* delete_node_at_beginning ( struct linklist* head )
{
    if (head == NULL)
    {
        printf ( "Linked list is empty - nothing to delete\n" );
        return head;
        return head;
    }
    else
    {
        struct linklist* temp;
        temp = head;
        head = head->next;
        free (temp);
        return head;
    }
}		/* -----  end of function delete_node_at_beginning  ----- */
