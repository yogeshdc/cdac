/*
 * =====================================================================================
 *
 *       Filename:  add_node_in_middle.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Saturday 02 September 2017 04:02:00  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */

#include        "linklist.h"
#include        "stdio.h"
#include        "stdlib.h"

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  add_node_in_middle
 *  Description:  
 * =====================================================================================
 */
void add_node_in_middle (struct linklist * head)
{
    int position;
    printf ( "Enter the position where the node needs to be added: \n" );
    scanf ( "%d", &position );


    for (int i=0; i<position-1; i++)
    {
        if (head == NULL)
        {
            printf ( "List does not have %d elements\n",position );
            return;
        }
        head = head->next;
    }
    
    if (head->next == NULL)
    {
        add_node_at_end(head);
    }
    else
    {
        struct linklist *newnode = create_newnode();
        newnode->next = head-> next;
        head->next = newnode;
    }
}		/* -----  end of function add_node_in_middle  ----- */
