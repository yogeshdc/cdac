/*
 * =====================================================================================
 *
 *       Filename:  link_main.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Saturday 02 September 2017 02:26:53  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */

#include        <stdio.h>
#include	<stdlib.h>
#include        "linklist.h"



/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
int main ( int argc, char *argv[] )
{
    struct linklist* head = NULL;
    char ch;
    int choice;

    do
    {
        printf ( "\n****MENU***\n" );
        printf ( "\n1. Display the link list...\n" );
        printf ( "\n2. Add the first node of linklist...\n" );
        printf ( "\n3. Add the node at end of linklist...\n" );
        printf ( "\n4. Add the node at beginning of linklist...\n" );
        printf ( "\n5. Add the node in middle of linklist...\n" );
        printf ( "\n6. Delete the node at start of linklist...\n" );
        printf ( "\n7. Delete the node at end of linklist...\n" );
        printf ( "\n8. Delete the node in middle of linklist...\n" );
        printf ( "\n9. Reverse the link list...\n" );
        printf ( "Enter your choice: \n" );\
        scanf ( "%d", &choice );

        switch ( choice ) 
        {
            case 1:	
                display_linklist(head);         
                break;
            case 2:	
                head = create_newnode();
                break;
           case 3:
                add_node_at_end(head);
               break;
           case 4:
                head = add_node_at_beginning(head);
               break;
           case 5:
                add_node_in_middle(head);
               break;
           case 6:
                head = delete_node_at_beginning(head);
               break;
           case 7:
                delete_node_at_end(head);
               break;
           case 8:
                delete_node_in_middle(head);
               break;
           case 9:
                head = reverse_linklist(head);
               break;
            default:	
                printf ( "Received invalid input\n" );
                break;
        }	/* -----  end switch  ----- */
        printf ( "Do you want to continue Y/N\n" );
        //Below 2 statements are to clear the input buffer
        fflush(stdin);
        scanf ( "%c", &ch );
        scanf ( "%c", &ch );
    } while (ch == 'y' || ch == 'Y');			
    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */
