/*
 * =====================================================================================
 *
 *       Filename:  display_node.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Saturday 02 September 2017 02:48:54  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include        "linklist.h"
#include        "stdio.h"
#include        "stdlib.h"

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  display_linklist
 *  Description:  
 * =====================================================================================
 */
void display_linklist (struct linklist * begin)
{
    if (begin == NULL)
    {
        printf ( "Link list is empty\n" );
    }
    else
    {
        printf ( "HEAD starts here ---->\n" );
        while(begin != NULL)
        {
            printf ( "\t\t%d\n",begin->data );
            printf ( "\t\t|\n" );
            printf ( "\t\t|\n" );
            printf ( "\t\tV\n" );
            begin = begin->next;
        }
        printf ( "\tEnd of linklist\n" );
    }
}		/* -----  end of function display_linklist  ----- */
