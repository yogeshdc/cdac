/*
 * =====================================================================================
 *
 *       Filename:  student.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Thursday 31 August 2017 07:05:05  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */
#pragma once
//#pragma pack(1)
struct student
{
    int roll_no;
    char name[11];
    double per;
};


void accept_input ( struct student* );
void display_struct ( struct student* );
