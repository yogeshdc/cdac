/*
 * =====================================================================================
 *
 *       Filename:  accept_input.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Thursday 31 August 2017 09:27:53  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */

#include        <stdio.h>
#include        "student.h"
/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  accept_input
 *  Description:  
 * =====================================================================================
 */
void accept_input ( struct student *s1, int num_students )
{
    int len;
    //pointer method
    for (int i=0; i<num_students; i++)
    {
        printf ( "Enter data for s1 student\n" );
        scanf ( "%d", &(s1+i)->roll_no );

        //Uncomment the below 3 lines if name is a char pointer
        //and wish to malloc memory for name variable
        
        //printf("Enter the length of name\n");
        //scanf("%d",&len);
        //(s1+i)->name = (char*)malloc(sizeof(char)*len);

        scanf ( "%s", (s1+i)->name );

        scanf ( "%lf", &(s1+i)->per );
    }
}		/* -----  end of function accept_input  ----- */


