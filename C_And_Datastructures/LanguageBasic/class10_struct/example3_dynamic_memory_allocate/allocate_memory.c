/*
 * =====================================================================================
 *
 *       Filename:  allocate_memory.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Friday 01 September 2017 08:04:52  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */
#include        <stdlib.h>
#include        "student.h"

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  allocate_memory
 *  Description:  
 * =====================================================================================
 */
void allocate_memory ( struct student** sptr, int num_students )
{
    *sptr = (struct student*) malloc( sizeof(struct student)*num_students);

}		/* -----  end of function allocate_memory  ----- */
