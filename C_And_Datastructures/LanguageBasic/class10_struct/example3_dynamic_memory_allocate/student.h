/*
 * =====================================================================================
 *
 *       Filename:  student.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Thursday 31 August 2017 07:05:05  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */
#pragma once
//#pragma pack(1)
struct student
{
    int roll_no;
    char name[11];
    double per;
};

void allocate_memory( struct student**, int);
void accept_input ( struct student*, int );
void display_struct ( struct student*, int );
