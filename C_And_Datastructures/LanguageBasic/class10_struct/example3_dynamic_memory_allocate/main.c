/*
 * =====================================================================================
 *
 *       Filename:  main.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Thursday 31 August 2017 08:29:05  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include	<stdlib.h>
#include	<stdio.h>
#include	<string.h>
#include	"student.h"

struct student assign_contents ( struct student s_to, struct student s_from );
struct student assign_contents_elementwise ( struct student s_to, struct student s_from);
//void assign_contents_pointer ( struct student *s_to, int to_index, int from_index );
void assign_contents_pointer ( struct student *s_to, struct student *s_from );
void assign_contents_pointer_elementwise ( struct student* s_to, struct student* s_from);
/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
int main ( int argc, char *argv[] )
{
    struct student *sptr;

    int num_students = 0;
    printf ( "How many students do you want in your records?\n" );
    scanf ( "%d", &num_students );

    //printf ( "Size of struct is %lu\n",sizeof(sptr) );
    //Send Pointer of struct method
    allocate_memory(&sptr, num_students);
    accept_input(sptr, num_students);
    display_struct(sptr, num_students);

    /*  
    //Copying by value
    //struct student s1;
    printf ( "Copying whole struct by value (whole struct at a time)\n" );
    //s1 = assign_contents(s1,*(sptr+1));
    *(sptr+0) = assign_contents(*(sptr+0),*(sptr+1));
    display_struct(sptr,3);


    printf ( "Copying whole struct by value (individual elements at one time)\n" );
    //Copying by value elemnentwise
    *(sptr+0) = assign_contents_elementwise(*(sptr+0),*(sptr+2));
    display_struct(sptr,3);
    */

    /*  
    printf ( "Copying whole struct by pointer (whole struct at one time)\n" );
    //Copying by pointer whole
    //assign_contents_pointer(sptr,1,2);
    assign_contents_pointer( (sptr+0),(sptr+1));
    display_struct(sptr,2);
    */

    printf ( "Copying whole struct by pointer (individual element at one time)\n" );
    assign_contents_pointer_elementwise( (sptr+0),(sptr+1));
    display_struct(sptr,2);
    
    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */



/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  assign_contents
 *  Description:  Assign the structure whole structure
 * =====================================================================================
 */
struct student assign_contents ( struct student s_to, struct student s_from )
{
    s_to = s_from; //This will assign the whole struct s1 to s2
    return s_to;
}		/* -----  end of function assign_contents  ----- */


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  assign_contents_elementwise
 *  Description:  
 * =====================================================================================
 */
struct student assign_contents_elementwise ( struct student s_to, struct student s_from )
{
    s_to.roll_no = s_from.roll_no;
    s_to.per = s_from.per;
    strcpy(s_to.name, s_from.name);
    return s_to;
}		/* -----  end of function assign_contents_elementwise  ----- */


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  assign_contents_pointer
 *  Description:  
 * =====================================================================================
 */
void assign_contents_pointer ( struct student *s_to, struct student *s_from)//int to_index, int from_index )
{
    *s_to = *s_from;
}		/* -----  end of function assign_contents_pointer  ----- */



// _____________TODO________________
// Add code for pointer assignment by individual value

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  assign_contents_pointer_elementwise
 *  Description:  
 * =====================================================================================
 */
void assign_contents_pointer_elementwise ( struct student* s_to, struct student* s_from )
{
    s_to->roll_no = s_from->roll_no;
    s_to->per = s_from->per;
    strcpy(s_to->name, s_from->name);
}		/* -----  end of function assign_contents_pointer_elementwise  ----- */
