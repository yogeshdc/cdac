/*
 * =====================================================================================
 *
 *       Filename:  display_struct.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Thursday 31 August 2017 09:28:44  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include        <stdio.h>
#include        "student.h"
/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  display_struct
 *  Description:  
 * =====================================================================================
 */

void display_struct (struct student *s1, int num_students )
{
    for (int i=0; i<num_students; i++)
    {
        printf ( "Roll no is: %d\n",(s1+i)->roll_no );
        printf ( "Name is: %s\n",(s1+i)->name );
        printf ( "Percent is: %lf\n",(s1+i)->per );
    }
}		/* -----  end of function display_struct  ----- */
