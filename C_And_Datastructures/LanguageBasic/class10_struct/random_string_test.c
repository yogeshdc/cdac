/*
 * =====================================================================================
 *
 *       Filename:  random_string_test.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Saturday 02 September 2017 11:04:29  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include	<stdlib.h>
#include	<stio.h>
#include	<string.h>


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
    int
main ( int argc, char *argv[] )
{
    char s1[15] = "someone";
    char s2[15];
    //s1 = s2; //This will give an error
    //
    strcpy(s2,s1);
    //Similarly for 2 structs s1 and s2 we cannot say
    //s1.name and s2.name
    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */
