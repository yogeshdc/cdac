/*
 * =====================================================================================
 *
 *       Filename:  main.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Thursday 31 August 2017 08:29:05  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include	<stdlib.h>
#include	<stdio.h>
#include	"student.h"

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
    int
main ( int argc, char *argv[] )
{
    struct student s1;
    struct student *sptr=NULL;
    printf("The size of struct s1 is: %lu\n",sizeof(s1));

    //Non pointer method
    s1 = accept_input(s1);
    display_struct(s1);

    //Pointer method
    /*
    sptr = (struct student *) malloc (sizeof(struct student));
    printf ( "Enter data for s2 student\n" );
    scanf ( "%d", sptr->roll_no );
    scanf ( "%s", sptr->name );
    scanf ( "%lf", sptr->per );
    printf ( "Roll no is: %d\n",sptr->roll_no );
    printf ( "Name is: %s\n",sptr->name );
    printf ( "Percent is: %lf\n",sptr->per );
    */


    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */

