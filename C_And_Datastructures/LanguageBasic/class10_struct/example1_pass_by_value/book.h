/*
 * =====================================================================================
 *
 *       Filename:  book.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Thursday 31 August 2017 07:05:22  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */

struct book
{
    int page_no;
    chae book_name[20];
    double price;
};
