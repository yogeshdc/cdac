/*
 * =====================================================================================
 *
 *       Filename:  accept_input.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Thursday 31 August 2017 09:27:53  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */

#include        <stdio.h>
#include        "student.h"
/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  accept_input
 *  Description:  
 * =====================================================================================
 */
struct student accept_input ( struct student s1 )
{
    //Non pointer method
    printf ( "Enter data for s1 student\n" );
    scanf ( "%d", &s1.roll_no );
    scanf ( "%s", s1.name );
    scanf ( "%lf", &s1.per );
    return s1;
}		/* -----  end of function accept_input  ----- */


