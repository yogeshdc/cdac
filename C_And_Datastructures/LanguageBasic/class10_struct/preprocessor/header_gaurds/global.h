/*
 * =====================================================================================
 *
 *       Filename:  global.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Thursday 31 August 2017 07:08:22  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */
#pragma once
//
//#ifndef GLOBAL_H    //Begin header gaurd
//#define GLOBAL_H

extern int g_val ;

//#endif              // End header gaurd
