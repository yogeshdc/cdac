/*
 * =====================================================================================
 *
 *       Filename:  display_right.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Thursday 31 August 2017 08:18:21  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */

#include	<stdio.h>
#include        "right.h"

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  display_right
 *  Description:  
 * =====================================================================================
 */
void display_right ()
{
    printf ( "right %d\n",g_val );
}		/* -----  end of function display_right  ----- */
