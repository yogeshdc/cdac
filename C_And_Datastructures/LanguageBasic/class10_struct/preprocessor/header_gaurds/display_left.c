/*
 * =====================================================================================
 *
 *       Filename:  display_left.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Thursday 31 August 2017 08:17:39  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */

#include	<stdio.h>
#include        "left.h"

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  display_left
 *  Description:  
 * =====================================================================================
 */
void display_left ()
{
    g_val = 99;
    printf ( "left %d\n",g_val );
}		/* -----  end of function display_left  ----- */



