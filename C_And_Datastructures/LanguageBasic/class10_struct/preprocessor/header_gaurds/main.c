/*
 * =====================================================================================
 *
 *       Filename:  main.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Thursday 31 August 2017 07:10:00  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include	<stdlib.h>
#include	<stdio.h>
#include        "left.h"
#include        "right.h"
int g_val = 30; //Defination in global scope is compulsory
/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
int main ( int argc, char *argv[] )
{
    g_val = 20; //Updation can be done in local socpe only after defination in global scope
    printf ( "main %d\n",g_val );
    display_left(); 
    g_val = 66;
    display_right();

    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */


