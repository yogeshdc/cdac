/*
 * =====================================================================================
 *
 *       Filename:  link_main.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Saturday 02 September 2017 02:26:53  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */

#include        <stdio.h>
#include	<stdlib.h>
#include        "doublylinklist.h"



/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
int main ( int argc, char *argv[] )
{
    struct doublylinklist* head = NULL;
    struct doublylinklist* tail = NULL;

    char ch;
    int choice;

    do
    {
        printf ( "\n\t\t\t\t\t****MENU***\n" );
        printf ( "\n1. Display the link list...\n" );
        printf ( "\n2. Add the first node of doublylinklist...\n" );
        printf ( "\n3. Add the node at end of doublylinklist...\n" );
        printf ( "\n4. Add the node at beginning of doublylinklist...\n" );
        printf ( "\n5. Add the node in middle of doublylinklist...\n" );
        printf ( "\n6. Delete the node at start of doublylinklist...\n" );
        printf ( "\n7. Delete the node at end of doublylinklist...\n" );
        printf ( "\n8. Delete the node in middle of doublylinklist...\n" );
        printf ( "\n9. Reverse the link list...\n" );
        printf ( "Enter your choice: \n" );\
        scanf ( "%d", &choice );

        switch ( choice ) 
        {
            case 1:	
                display_doublylinklist(&head, &tail);         
                break;
            case 2:	
                if (head == NULL)
                    create_newnode(&head, &tail);
                else
                    printf ( "List is not empty\n" );
                break;
           case 3:
                add_node_at_end(&head, &tail);
               break;
           case 4:
                add_node_at_beginning(&head, &tail);
               break;
           case 5:
                add_node_in_middle(&head, &tail);
               break;
           case 6:
                delete_node_at_beginning(&head, &tail);
               break;
           case 7:
                delete_node_at_end(&head, &tail);
               break;
           case 8:
                delete_node_in_middle(&head, &tail);
               break;
           case 9:
                reverse_doublylinklist(&head, &tail);
               break;
            default:	
                printf ( "Received invalid input\n" );
                break;
        }	/* -----  end switch  ----- */
        printf ( "Do you want to continue Y/N\n" );
        //Below 2 statements are to clear the input buffer
        fflush(stdin);
        scanf ( "%c", &ch );
        scanf ( "%c", &ch );
    } while (ch == 'y' || ch == 'Y');			
    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */
