/*
 * =====================================================================================
 *
 *       Filename:  create_newnode.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Saturday 02 September 2017 02:40:06  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */

#include        "doublylinklist.h"
#include        "stdio.h"
#include        "stdlib.h"

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  create_newnode
 *  Description:  
 * =====================================================================================
 */
struct doublylinklist* create_newnode (struct doublylinklist** head, struct doublylinklist** tail)
{
    struct doublylinklist* node = NULL;

    //allocate a memory for single strucure variable(NODE)
    node = (struct doublylinklist*) malloc (sizeof(struct doublylinklist));

    //accept the value for doublylinklist data
    printf ( "Enter the data (integer) for the node\n" );
    scanf ( "%d", &node->data );

    //assign NULL to next pointer
    head->next = NULL;
    head->prev = NULL;
    if (*head == NULL && *tail == NULL)
    {
        *head = node;
        *tail = node;
    }
    return *head;
}		/* -----  end of function create_newnode  ----- */
