/*
 * =====================================================================================
 *
 *       Filename:  delete_node_at_end.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Saturday 02 September 2017 04:58:34  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */

#include        <stdlib.h>
#include        <stdio.h>
#include        "linklist.h"

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  delete_node_at_end
 *  Description:  
 * =====================================================================================
 */
void delete_node_at_end ( struct linklist* head )
{
    if (head == NULL)
    {
        printf ( "Linked list is empty - nothing to delete\n" );
        return;
    }
    if (head->next == NULL)
    {
        printf ( "The list has only one node\n" );
        free(head);
    }
    else
    {
        while (head->next->next!=NULL)
        {
            head = head->next;
        }

        free(head->next->next);
        head->next = NULL;

    }
}		/* -----  end of function delete_node_at_end  ----- */
