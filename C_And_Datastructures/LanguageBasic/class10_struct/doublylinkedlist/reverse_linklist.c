/*
 * =====================================================================================
 *
 *       Filename:  reverse_linklist.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Saturday 02 September 2017 05:40:20  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */

#include        <stdio.h>
#include        <stdlib.h>
#include        "linklist.h"

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  reverse_linklist
 *  Description:  
 * =====================================================================================
 */
struct linklist* reverse_linklist ( struct linklist* head )
{

    if (head == NULL)
    {
        printf ( "List is empty nothing to reverse. \n" );
    }
    else if (head->next == NULL)
    {
        printf ( "List has only one element. Nothing to reverse\n" );
    }
    else
    {
        struct linklist* current = head;
        struct linklist* one_step_forward = head->next;
        struct linklist* two_step_forward = head->next->next;

        current->next = NULL;
        one_step_forward->next = current;
        while(two_step_forward != NULL)
        {
            current = one_step_forward;
            one_step_forward = two_step_forward;
            two_step_forward= two_step_forward->next;
            one_step_forward->next = current;
        }
        head = one_step_forward;
    }
    return head;
}		/* -----  end of function reverse_linklist  ----- */
