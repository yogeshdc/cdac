/*
 * =====================================================================================
 *
 *       Filename:  doublylinklist.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Saturday 02 September 2017 02:24:18  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */
#pragma once

struct doublylinklist
{
    int data;
    struct doublylinklist *prev;
    struct doublylinklist *next;
};

struct doublylinklist* create_newnode ();
void display_doublylinklist (struct doublylinklist* );
void add_node_at_end (struct doublylinklist * );
void add_node_in_middle (struct doublylinklist * );
struct doublylinklist* add_node_at_beginning (struct doublylinklist * );
struct doublylinklist* delete_node_at_beginning (struct doublylinklist *);
struct doublylinklist* reverse_doublylinklist (struct doublylinklist *);
void delete_node_at_end (struct doublylinklist *);
void delete_node_in_middle ( struct doublylinklist* );
