/*
 * =====================================================================================
 *
 *       Filename:  add_node_at_beginning.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Saturday 02 September 2017 03:44:34  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */



#include        "linklist.h"
#include        "stdio.h"
#include        "stdlib.h"

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  add_node_at_beginning
 *  Description:  
 * =====================================================================================
 */
struct linklist* add_node_at_beginning (struct linklist * head)
{
    if (head == NULL)
    {
        printf ( "List is empty\n" );
        head = create_newnode();
        return head;
    }

    else
    {
        struct linklist *newhead = create_newnode();
        newhead->next = head;

        head = newhead;
        return head;
    }
}		/* -----  end of function add_node_at_beginning  ----- */
