/*
 * =====================================================================================
 *
 *       Filename:  add_node_at_end.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Saturday 02 September 2017 03:31:35  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include        "doublylinklist.h"
#include        "stdio.h"
#include        "stdlib.h"

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  add_node_at_end
 *  Description:  
 * =====================================================================================
 */
void add_node_at_end (struct doublylinklist ** head, struc doublylinklis **tail)
{
    if (tail == NULL && head == NULL)
    {
        printf ( "List is empty\n" );
        printf ( "Adding first element\n" );
        create_newnode(head, tail);
        return;
    }
    else
    {
        struct doublylinklis *newnode = NULL;
        create_newnode(&newnode );
        *tail = newnode;
    }
}		/* -----  end of function create_newnode  ----- */
