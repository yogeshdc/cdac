/*
 * =====================================================================================
 *
 *       Filename:  forloop.c
 *
 *    Description:  Preliminary Loop example
 *
 *        Version:  1.0
 *        Created:  Wednesday 23 August 2017 09:14:48  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include	<stdlib.h>
#include	<stdio.h>

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  For loop exaples
 * =====================================================================================
 */
int main ( int argc, char *argv[] )
{
    int number;

    printf ( "Enter the number whose multiplication table needs to be printed:\n" );
    scanf ( "%d", &number );

    for ( int i=1; i<=12; i++ ) 
    {
        printf ( "\t\t %7d * %3d = %15d \n",number,i,number*i );
    }

    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */
