/*
 * =====================================================================================
 *
 *       Filename:  armstrong.c
 *
 *    Description:  Find if a number is armstrong
 *
 *        Version:  1.0
 *        Created:  Wednesday 23 August 2017 08:38:54  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include	<stdlib.h>
#include	<stdio.h>
#include        <math.h>
int calculate_digits (int);
int calculate_sum_of_digits ( int , int );

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
int main ( int argc, char *argv[] )
{
    int number, num_digits, sum , temp;
    printf ("Enter an integer to find if its armstrong or not \n");
    scanf ("%d",&number);
    temp = number;

    num_digits = calculate_digits(number);

    sum = calculate_sum_of_digits ( number, num_digits );

    if (sum == number)
    {
        printf ( "%d is an armstrong number\n",number );
    }

    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  calculate_digits
 *  Description:  
 * =====================================================================================
 */

int calculate_digits ( int number )
{
    int digits = 0;
    while (number!=0)
    {
        digits +=1;
        number/=10;
    }
    return digits;
}		/* -----  end of function calculate_digits  ----- */


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  calculate_sum_of_digits
 *  Description:  
 * =====================================================================================
 */
int calculate_sum_of_digits ( int number, int num_digits )
{
    int sum = 0;
    for (int i=1; i<=num_digits; i++)
    {
        sum += pow(number%10,num_digits);
        number/=10;
    }
    return sum;
}		/* -----  end of function calculate_sum_of_digits  ----- */
