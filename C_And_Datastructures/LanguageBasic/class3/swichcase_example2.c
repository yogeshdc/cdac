/*
 * =====================================================================================
 *
 *       Filename:  forloop.c
 *
 *    Description:  SwitchCase Example
 *
 *        Version:  1.0
 *        Created:  Wednesday 23 August 2017 08:02:56  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include	<stdlib.h>
#include	<stdio.h>
int switchcaseexample (int );
/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
int main ( int argc, char *argv[] )
{
    int choice, num1, num2;

    printf ( "\t\t\t\t***MENU***\n" );
    printf ( "1.Science\n2.Commerce\n3.Arts\n" );
    printf ( "Enter your choice (1-3): \n" );
    scanf ( "%d",&choice);
    printf ( "\t\t\t\t**********\n" );

    switchcaseexample (choice);
    //printf ( "Result = %d\n", result );

    return EXIT_SUCCESS;
}/* ----------  end of function main  ---------- */




/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  switchcaseexample
 *  Description:  Preliminary example of case statement
 * =====================================================================================
 */
int switchcaseexample ( int choice )
{
    int result = -1;
    int sub = 0;
    switch ( choice ) {
        case 1:	
            printf ( "\t\t\t\t***SUBJECTS***\n");
            printf ( "1.Biology\n2.Physics\n3.Chemistry\n4.Maths\n" );
            printf ( "Enter your subject (1-4)\n" );
            scanf ( "%d", &sub );
            
            switch ( sub ) {
                case 1:
                    printf ( "Welcome to Bio Dept\n" );
                    break;

                case 2:	
                    printf ( "Welcome to Physics Dept\n" );
                    break;

                case 3:	
                    printf ( "Welcome to Chemistry Dept\n" );
                    break;

                case 4:	
                    printf ( "Welcome to Maths Dept\n" );
                    break;

                default:
                    printf ( "Received invalid input\n" );
                    break;
            }	
            /* -----  end internal switch  ----- */
            
            break;

        case 2:
            printf ( "Welcome to Commerce Dept\n" );




            printf ( "\t\t\t\t***SUBJECTS***\n");
            printf ( "1.Finance\n2.Accounts\n3.Economics\n" );
            printf ( "Enter your subject (1-3)\n" );
            scanf ( "%d", &sub );
            
            switch ( sub ) {
                case 1:
                    printf ( "Welcome to Finance Dept\n" );
                    break;

                case 2:	
                    printf ( "Welcome to Accounts Dept\n" );
                    break;

                case 3:	
                    printf ( "Welcome to Economics Dept\n" );
                    break;

                default:
                    printf ( "Received invalid input\n" );
                    break;
            }
            /* -----  end internal switch  ----- */

            break;

        case 3:	
            printf ( "Welcome to Arts Depratment\n" );
            
            
            
            printf ( "\t\t\t\t***SUBJECTS***\n");
            printf ( "1.Fine Arts\n2.History\n3.Social Science\n" );
            printf ( "Enter your subject (1-3)\n" );
            scanf ( "%d", &sub );
            
            switch ( sub ) {
                case 1:
                    printf ( "Welcome to Fine arts Dept\n" );
                    break;

                case 2:	
                    printf ( "Welcome to History Dept\n" );
                    break;

                case 3:	
                    printf ( "Welcome to Social Sci Dept\n" );
                    break;

                default:
                    printf ( "Received invalid input\n" );
                    break;
            }
            /* -----  end internal switch  ----- */

            break;

        default:
            printf ( "Invalid input received\n" );
            break;
    }				/* -----  end switch  ----- */
    return result;
}		/* -----  end of function switchcaseexample  ----- */
            
