/*
 * =====================================================================================
 *
 *       Filename:  gracemarks.c
 *
 *    Description:  Write a program which to find the grace marks for a student
 *    using switch. The user should enter the class obtained by the
 *    student and the number of subjects he has failed in.
 *
 *    − If the student gets first class and the number of subjects he
 *    failed in is greater than 3, then he does not get any grace.
 *    If the number of subjects he failed in is less than or equal
 *    to 3 then the grace is of 5 marks per subject.
 *    − If the student gets second class and the number of subjects
 *    he failed in is greater than 2, then he does not get any
 *    grace. If the number of subjects he failed in is less than or
 *    equal to 2 then the grace is of 4 marks per subject.
 *    − If the student gets third class and the number of subjects
 *    he failed in is greater than 1, then he does not get any
 *    grace. If the number of subjects he failed in is equal to 1
 *    then the grace is of 5 marks per subject
 *
 *        Version:  1.0
 *        Created:  Wednesday 23 August 2017 07:02:04  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include	<stdlib.h>
#include        <stdio.h>

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
    int
main ( int argc, char *argv[] )
{
    int class, failed, grace_marks;
    printf ( "\t\t\t ***MENU***\n" );
    printf ( "1 stands for first class\n" );
    printf ( "2 stands for second class\n" );
    printf ( "3 stands for third class\n" );
    printf("Enter the final class of the student: ");
    scanf ( "%d", &class );
    printf("Enter the number of subjects failed: ");
    scanf ( "%d", &failed );
    
    
    switch ( class ) {
        case 1:	
            
            switch ( failed ) {
                case 1:	
                case 2:	
                case 3:
                    grace_marks = failed*5;
                    printf ( "The grace marks given are: %d\n", grace_marks );
                    break;

                default:
                    printf("No grace marks given\n");
                    break;
            }				/* -----  end switch  ----- */
            break;

        case 2:	

            switch ( failed ) {
                case 1:	
                case 2:	
                    grace_marks = failed*4;
                    printf ( "The grace marks given are: %d\n", grace_marks );
                    break;

                default:
                    printf("No grace marks given\n");
                    break;
            }				/* -----  end switch  ----- */

            break;

        case 3:	

            switch ( failed ) {
                case 1:	
                    grace_marks = failed*5;
                    printf ( "The grace marks given are: %d\n", grace_marks );
                    break;

                default:
                    printf("No grace marks given\n");
                    break;
            }				/* -----  end switch  ----- */

            break;

        default:
            printf ( "Invalid class received\n" );
            break;
    }				/* -----  end switch  ----- */

    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */
