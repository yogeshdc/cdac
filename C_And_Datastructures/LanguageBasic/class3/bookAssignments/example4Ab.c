/*
 * =====================================================================================
 *
 *       Filename:  example4Aa.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Wednesday 23 August 2017 06:52:07  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include	<stdlib.h>
#include	<stdio.h>


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
int main ( int argc, char *argv[] )
{
    int c = 3,d ;
    switch ( d=c+1 )
    {
        case 'v' :
            printf ( "I am in case v \n" ) ;
            break ;
        case 3 :
            printf ( "I am in case 3 \n" ) ;
            break ;
        case 4 :
            printf ( "I am in case 4 \n" ) ;
            break ;
        default :
            printf ( "I am in default \n" ) ;
    }
    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */
