/*
 * =====================================================================================
 *
 *       Filename:  example4Aa.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Wednesday 23 August 2017 06:52:07  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include	<stdlib.h>
#include	<stdio.h>


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
int main ( int argc, char *argv[] )
{

    int i = 0 ;
    switch ( i )
    {
        case 0 :
            printf ( "\nCustomers are dicey" ) ;
        case 1 :
            printf ( "\nMarkets are pricey" ) ;
        case 2 :
            printf ( "\nInvestors are moody" ) ;
        case 3 :
            printf ( "\nAt least employees are good" ) ;
    }
    
    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */
