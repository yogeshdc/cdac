/*
 * =====================================================================================
 *
 *       Filename:  matchstickgame.c
 *
 *    Description:  Game of matches
 *
 *        Version:  1.0
 *        Created:  Wednesday 23 August 2017 07:36:47  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include	<stdlib.h>
#include	<stdio.h>
#include	<assert.h>

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
int main ( int argc, char *argv[] )
{
    int user_choice = 0, computer_choice = 0;
    int remaining_matches = 21;
    printf ( "Lets play the Matchstick Game. Whoever picks the last matchstick looses\n" );
    printf ( "User and computer can bothe pick between 1 to 4 matchsticks from a total lot of 21\n" );
    printf ( "Lets Start\n"); 

    while(remaining_matches>0) 
    {
        printf ( "Pick the number of matchsticks you want to pick\n");
        scanf ( "%d",&user_choice);
        assert (user_choice <=4 && user_choice >=1);
        assert (user_choice <=remaining_matches);
        remaining_matches -= user_choice;
        if (remaining_matches<=0) 
        {
            printf ( "Computer Wins\n" );
            break;
        }
        computer_choice = 5 - user_choice;
        printf ( "Computer chooses %d matches\n", computer_choice );
        remaining_matches -= computer_choice;
        if (remaining_matches<=0) 
        {
            printf ( "User Wins\n" );
            break;
        }
        printf ( "Matches Remaining after this round: %d\n",remaining_matches );
    }
    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */



