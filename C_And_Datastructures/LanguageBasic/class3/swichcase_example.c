/*
 * =====================================================================================
 *
 *       Filename:  forloop.c
 *
 *    Description:  ForLoopExample
 *
 *        Version:  1.0
 *        Created:  Wednesday 23 August 2017 08:02:56  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include	<stdlib.h>
#include	<stdio.h>
int switchcaseexample (int, int, int);
/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
int main ( int argc, char *argv[] )
{
    int choice, num1, num2;

    printf ( "Enter 2 integers\n" );
    scanf ( "%d %d", &num1, &num2 );
    printf ( "\t\t\t\t***MENU***\n" );
    printf ( "1.Addition\n2.Subtraction\n3.Multiplication\n4.Division\n" );
    printf ( "Enter your choice (1-4): \n" );
    scanf ( "%d",&choice);
    printf ( "\t\t\t\t**********\n" );

    int result = switchcaseexample (choice, num1, num2);
    printf ( "Result = %d\n", result );








    return EXIT_SUCCESS;
}/* ----------  end of function main  ---------- */




/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  switchcaseexample
 *  Description:  Preliminary example of case statement
 * =====================================================================================
 */
int switchcaseexample ( int choice, int num1, int num2 )
{
    int result = -1;
    switch ( choice ) {
        case 1:	
            printf ( "Addition of %d and %d: ", num1, num2 );
            result = num1+num2;
            break;

        case 2:	
            printf ( "Subtraction of %d and %d: ", num1, num2 );
            result = num1-num2;
            break;

        case 3:	
            printf ( "Multiplication of %d and %d: ", num1, num2 );
            result = num1*num2;
            break;

        case 4:
            printf ( "Division of %d and %d: ", num1, num2 );
            result = num1/num2;
            break;

        default:
            printf ( "Invalid input received\n" );
            break;
    }				/* -----  end switch  ----- */
    return result;
}		/* -----  end of function switchcaseexample  ----- */
