/*
 * =====================================================================================
 *
 *       Filename:  reverse_n_digits.c
 *
 *    Description:  Reverse the digits of a number with any nymber of digits
 *
 *        Version:  1.0
 *        Created:  Wednesday 23 August 2017 07:23:27  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include	<stdlib.h>
#include	<stdio.h>

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
int main ( int argc, char *argv[] )
{

    int n,rev=0;
    printf("Enter a no:");
    scanf("%d",&n);
    while(n!=0)
    {
        rev=rev*10+n%10;
        n=n/10;
    }
    printf("%d\n",rev);
    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */
