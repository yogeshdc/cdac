/*
 * =====================================================================================
 *
 *       Filename:  delete_node_in_middle.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Saturday 02 September 2017 05:09:39  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */

#include        <stdlib.h>
#include        <stdio.h>
#include        "linklist.h"

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  delete_node_in_middle
 *  Description:  
 * =====================================================================================
 */
void delete_node_in_middle ( struct linklist* head )
{
    int position;
    printf ( "Enter the position you want to delete \n" );
    scanf ( "%d", &position );
    
    for (int i=0; i<position-1; i++)
    {
        if (head == NULL)
        {
            printf ( "List does not have %d elements\n",position );
            return;
        }
        head = head->next;
    }
 
    if (head->next == NULL)
    {
        delete_node_at_end(head);
    }
    else
    {
        struct linklist *temp;
        temp = head->next;
        head->next = head->next->next;
        free(temp);
    }

    return;
}		/* -----  end of function delete_node_in_middle  ----- */
