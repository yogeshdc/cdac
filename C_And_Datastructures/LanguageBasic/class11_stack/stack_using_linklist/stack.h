/*
 * =====================================================================================
 *
 *       Filename:  stack.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Monday 04 September 2017 07:03:20  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */

#pragma once

struct stack
{
    int data;
    struct stack *next;
};

struct stack* create_newnode ();
void display_stack (struct stack* );
void add_node_at_end (struct stack * );
void delete_node_at_end (struct stack *);
