/*
 * =====================================================================================
 *
 *       Filename:  create_newnode.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Saturday 02 September 2017 02:40:06  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */

#include        "linklist.h"
#include        "stdio.h"
#include        "stdlib.h"

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  create_newnode
 *  Description:  
 * =====================================================================================
 */
struct linklist* create_newnode ()
{
    struct linklist* head = NULL;

    //allocate a memory for single strucure variable(NODE)
    head = (struct linklist*) malloc (sizeof(struct linklist));

    //accept the value for linklist data
    printf ( "Enter the data (integer) for the node\n" );
    scanf ( "%d", &head->data );

    //assign NULL to next pointer
    head->next = NULL;
    return head;
}		/* -----  end of function create_newnode  ----- */
