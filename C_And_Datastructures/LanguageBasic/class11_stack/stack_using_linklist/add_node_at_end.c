/*
 * =====================================================================================
 *
 *       Filename:  add_node_at_end.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Saturday 02 September 2017 03:31:35  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include        "linklist.h"
#include        "stdio.h"
#include        "stdlib.h"

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  add_node_at_end
 *  Description:  
 * =====================================================================================
 */
void add_node_at_end (struct linklist * head)
{
    if (head == NULL)
    {
        printf ( "List is empty\n" );
        return;
    }


    //move to end of node
    while(head->next != NULL)
        head = head->next;

    //allocate a memory for single strucure variable(NODE)
    head->next = (struct linklist*) malloc (sizeof(struct linklist));

    //accept the value for linklist data
    printf ( "Enter the data (integer) for the node\n" );
    scanf ( "%d", &head->next->data );

    //assign NULL to next pointer
    head->next->next = NULL;
}		/* -----  end of function create_newnode  ----- */
