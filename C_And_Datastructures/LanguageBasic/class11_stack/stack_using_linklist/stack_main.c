/*
 * =====================================================================================
 *
 *       Filename:  stack_main.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Monday 04 September 2017 07:02:57  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include        <stdio.h>
#include	<stdlib.h>
#include        "stack.h"

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
int main ( int argc, char *argv[] )
{
    struct stack* head = NULL;
    char ch;
    int choice;

    do
    {
        printf ( "\n****MENU***\n" );
        printf ( "\n1. Display the stack...\n" );
        printf ( "\n2. Push the first node of stack...\n" );
        printf ( "\n3. Push the node at end of stack...\n" );
        printf ( "\n4. Pop the node at end of stack...\n" );
        printf ( "Enter your choice: \n" );\
        scanf ( "%d", &choice );

        switch ( choice ) 
        {
            case 1:	
                display_stack(head);         
                break;
            case 2:	
                head = create_newnode();
                break;
           case 3:
                add_node_at_end(head);
               break;
           case 4:
                delete_node_at_end(head);
               break;
           default:	
                printf ( "Received invalid input\n" );
                break;
        }	/* -----  end switch  ----- */
        printf ( "Do you want to continue Y/N\n" );
        //Below 2 statements are to clear the input buffer
        fflush(stdin);
        scanf ( "%c", &ch );
        scanf ( "%c", &ch );
    } while (ch == 'y' || ch == 'Y');			
    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */
