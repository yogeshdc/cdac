/*
 * =====================================================================================
 *
 *       Filename:  stack.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Monday 04 September 2017 07:42:19  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */

#pragma oncie
#define         SIZE    5
#include        <stdbool.h>
#include        <stdio.h>

struct Stack
{
    int arr[SIZE];
    int top;
};

bool IsFull (struct Stack*);
bool IsEmpty (struct Stack*);
void Push (int, struct Stack*);
int Pop (struct Stack*);
void Peek(struct Stack*);
