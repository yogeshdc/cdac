/*
 * =====================================================================================
 *
 *       Filename:  stack_main.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Monday 04 September 2017 08:10:37  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include	<stdlib.h>
#include	<stdio.h>
#include	<stdbool.h>
#include        "Stack.h"


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
int main ( int argc, char *argv[] )
{
    int choice, element;
    char ch;
    struct Stack s1;
    s1.top = 0; //-1;
    struct Stack *s = &s1;

    do
    {
        printf ( "\t\t\t*****MENU*****\n" );
        printf ( "1. Check if stack is full.\n" );
        printf ( "2. Check if stack is IsEmpty.\n" );
        printf ( "3. Push element to Stack top.\n" );
        printf ( "4. Pop element from stack top.\n" );
        printf ( "5. Peek top of the stack.\n" );
        printf ( "Enter your choice:\n" );
        scanf ( "%d", &choice );

        switch ( choice ) {
            case 1:	
                if(IsFull(s))
                {
                    printf ( "Stack is Full\n" );
                }
                else
                {
                    printf ( "Stack is not Full\n" );
                }
                break;

            case 2:	
                if(IsEmpty(s))
                {
                    printf ( "Stack is Empty\n" );
                }
                else
                {
                    printf ( "Stack is not Empty\n" );
                }
                break;

            case 3:
                printf ( "Enter the element you want to push\n" );
                scanf ( "%d", &element );
                Push(element, s);
                break;

            case 4:	
                element = Pop(s);
                if (element != -1)
                {
                    printf ( "Elememt popped is: %d\n",element );
                }
                break;

            case 5:
                Peek(s);
                break;

            default:
                printf ( "Received invalid input\n" );
                break;
        }				/* -----  end switch  ----- */

        printf ( "Do you want to continue?\n" );
        fflush(stdin);
        scanf ( "%c", &ch );
        scanf ( "%c", &ch );

    } while (ch == 'y' || ch == 'Y');

    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */
