/*
 * =====================================================================================
 *
 *       Filename:  transpose_matrix.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Tuesday 29 August 2017 07:41:57  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  transpose_matrix
 *  Description:  
 * =====================================================================================
 */
    void
transpose_matrix ( int matrix[][3] )
{
    int transpose[3][3];
    
    for ( int i=0; i<3 ;i++ )
    {
        for ( int j=0; j<3 ;j++ )
        {
            transpose[i][j] = matrix[j][i];
        }
    }
    for ( int i=0; i<3 ;i++ )
    {
        for ( int j=0; j<3 ;j++ )
        {
            matrix[i][j] = transpose[i][j];
        }
    }
}		/* -----  end of function transpose_matrix  ----- */
