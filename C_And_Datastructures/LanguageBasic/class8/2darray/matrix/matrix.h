/*
 * =====================================================================================
 *
 *       Filename:  matrix.h
 *
 *    Description
 *
 *        Version:  1.0
 *        Created:  Tuesday 29 August 2017 07:31:13  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */

void display_line();
void accept_input(int[][3], int, int );
void display_matrix(int[][3], int, int );
void add_matrix ( int matrix[][3], int matrix2[][3], int matrix3[][3], int row_size, int col_size );
void sub_matrix ( int matrix[][3], int matrix2[][3], int matrix3[][3], int row_size, int col_size );
void multiply_matrix ( int matrix[][3], int matrix2[][3], int matrix3[][3], int row_size, int col_size );
void transpose_matrix (int matrix[][3] );
