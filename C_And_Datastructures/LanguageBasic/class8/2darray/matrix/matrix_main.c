/*
 * =====================================================================================
 *
 *       Filename:  matrix_main.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Tuesday 29 August 2017 08:31:02  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include	<stdlib.h>
#include	<stdio.h>
#include        "matrix.h"


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
    int
main ( int argc, char *argv[] )
{
    const int row_size = 3;
    const int col_size =3;
    int matrix[row_size][col_size];
    int matrix2[row_size][col_size];
    int matrix3[row_size][col_size];

    accept_input ( matrix, row_size, col_size );
    accept_input ( matrix2, row_size, col_size );

    display_line();
    display_matrix ( matrix, row_size, col_size );

    display_line();
    display_matrix ( matrix2, row_size, col_size );

    display_line();

    add_matrix ( matrix, matrix2, matrix3, row_size, col_size);
    display_matrix ( matrix3, row_size, col_size );
    display_line();

    sub_matrix ( matrix, matrix2, matrix3, row_size, col_size);
    display_matrix ( matrix3, row_size, col_size );
    display_line();

    multiply_matrix ( matrix, matrix2, matrix3, row_size, col_size);
    display_matrix ( matrix3, row_size, col_size );
    display_line();

    transpose_matrix(matrix);
    display_matrix ( matrix, row_size, col_size );
    display_line();



    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */




