/*
 * =====================================================================================
 *
 *       Filename:  accept_input.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Tuesday 29 August 2017 07:33:35  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */

#include <stdio.h>

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  accept_input
 *  Description:  
 * =====================================================================================
 */
    void
accept_input ( int matrix[][3], int row_size, int col_size )
{
    for ( int i=0; i<row_size ;i++ )
    {
        for ( int j=0; j<col_size ;j++ )
        {
            scanf ( "%d", &matrix[i][j] );
        }
    }
}		/* -----  end of function accept_input  ----- */


