/*
 * =====================================================================================
 *
 *       Filename:  sub_matrix.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Tuesday 29 August 2017 07:36:37  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */
#include <stdio.h>

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  sub_matrix
 *  Description:  
 * =====================================================================================
 */
    void
sub_matrix ( int matrix[][3], int matrix2[][3], int matrix3[][3], int row_size, int col_size )
{
    for ( int i=0; i<row_size ;i++ )
    {
        printf ( "\n" );
        for ( int j=0; j<col_size ;j++ )
        {
            matrix3[i][j] = matrix[i][j] - matrix2[i][j];
        }
    }
}		/* -----  end of function sub_matrix  ----- */


