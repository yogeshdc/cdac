/*
 * =====================================================================================
 *
 *       Filename:  display_line.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Tuesday 29 August 2017 07:38:01  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */
#include <stdio.h>

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  display_line
 *  Description:  
 * =====================================================================================
 */
void display_line ( )
{
    printf ( "\n" );
    for ( int i=0; i<80; i++ )
        printf ( "*" );
    printf ( "\n" );
}		/* -----  end of function display_line  ----- */
