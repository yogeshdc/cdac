/*
 * =====================================================================================
 *
 *       Filename:  multiply_matrix.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Tuesday 29 August 2017 07:37:29  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */



/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  multiply_matrix
 *  Description:  
 * =====================================================================================
 */
    void
multiply_matrix ( int matrix[][3], int matrix2[][3], int matrix3[][3], int row_size, int col_size )
{
    for (int i=0; i<col_size; i++)
    {
        for (int j=0; j<col_size; j++)
        {
            matrix3[i][j] = 0;
            for (int k=0; k<col_size; k++)
                matrix3[i][j] += matrix[i][k]*matrix2[k][j];
        }
    }
}		/* -----  end of function multiply_matrix  ----- */



