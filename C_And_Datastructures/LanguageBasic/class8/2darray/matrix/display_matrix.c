/*
 * =====================================================================================
 *
 *       Filename:  display_matrix.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Tuesday 29 August 2017 07:34:06  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */

#include <stdio.h>

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  display_matrix
 *  Description:  
 * =====================================================================================
 */
    void
display_matrix ( int matrix[][3], int row_size, int col_size )
{
     for ( int i=0; i<row_size ;i++ )
     {
         printf ( "\n" );
        for ( int j=0; j<col_size ;j++ )
        {
            printf ( "%d\t", *(*(matrix+i)+j) );
        }
    }
}		/* -----  end of function display_matrix  ----- */


