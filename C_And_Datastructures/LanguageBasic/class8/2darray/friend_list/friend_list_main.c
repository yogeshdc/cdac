/*
 * =====================================================================================
 *
 *       Filename:  friend_list_main.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Tuesday 29 August 2017 07:58:25  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include	<stdlib.h>
#include	<stdio.h>
#include	<string.h>

void accept_names ( char names[][20], int num_friends );
void display_names ( char names[][20], int num_friends );
/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
    int
main ( int argc, char *argv[] )
{
    char name[10][20];
    int num_friends;
    printf ( "Enter the number of friends (less than 10): \n" );
    scanf ( "%d", &num_friends );
    accept_names(name, num_friends);
    display_names(name, num_friends);

    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */




/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  accept_names
 *  Description:  
 * =====================================================================================
 */
void accept_names ( char name[][20], int num_friends )
{
    for ( int i =0; i<num_friends ;i++ )
    {
        printf ( "Enter the name of friend %d: \n",i );
        scanf ( "%s", name[i] );
    }

}		/* -----  end of function accept_names  ----- */


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  display_names
 *  Description:  
 * =====================================================================================
 */
void display_names ( char name[][20], int num_friends )
{
    for ( int i =0; i<num_friends ;i++ )
    {
        printf ( "friend %d: %s \n",i,name[i] );
    }
}		/* -----  end of function display_names  ----- */



/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  sort_array
 *  Description:  
 * =====================================================================================
 */
void sort_array ( char[][20], int num_friends )
{
    for (int i=0; i<num_friends; i++)
    {
        for (int j=i; j<num_friends; j++)
        {
            if (strcmp(name[i], name[j]) > 0)
            {
                swap_strings(name[i],name[i+1])
            }
        }
    }
}		/* -----  end of function sort_array  ----- */



/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  swap_strings
 *  Description:  
 * =====================================================================================
 */
void swap_strings ( char* str1, char*str2 )
{
    int i=0;
  //****************************************TODO _ COMPLETE THIS CODE********************  
    return <+return_value+>;
}		/* -----  end of function swap_strings  ----- */
