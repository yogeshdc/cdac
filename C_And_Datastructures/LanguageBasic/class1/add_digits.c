/*
* Requirement: Add individual digits of one 4 digit numbers and display the result
* 
* 1. Accept 2 numbers from user
* 2. Set Sum to 0
* 3. Get the last digit using modulo operator and add it to sum
* 4. repeat 3 till all digits are added
*
*
*
*/


# include <stdio.h>

int main()
{
	int num, sum=0;
	scanf ("%d", &num);
	for (int i=0;i<=3;i++) 
	{
		sum = sum + (num%10);
		num = num/10;
	}
	printf("Sum = %d\n\n", sum);
}
