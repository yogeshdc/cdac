//Addition of 2 numbers

#include <stdio.h>

int main()
{
	int num1, num2, res;
	printf ("Enter 2 numbers:");
	scanf ("%d %d", &num1, &num2);
	res = num1 + num2;
	printf ("Sum = %d\n\n", res);
	return 0;
}
