/*
 * =====================================================================================
 *
 *       Filename:  printpatterns.c
 *
 *    Description:  Print some patterns of numbers
 *
 *        Version:  1.0
 *        Created:  Thursday 24 August 2017 07:43:55  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include	<stdlib.h>
#include	<stdio.h>

void print_right_pointing_tree(int);
void print_left_pointing_tree(int);
void print_both_sides_pointing_tree(int);
void print_right_pointing_tree_of_numbers(int);
void print_left_pointing_tree_of_numbers(int);
void print_both_sides_pointing_tree_of_numbers(int);
/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
int main ( int argc, char *argv[] )
{
    int size = 0;
    printf ( "Enter the length of the tree required\n" );
    scanf ( "%d", &size );
    print_right_pointing_tree(size);
    print_left_pointing_tree(size);
    print_both_sides_pointing_tree(size);
    print_right_pointing_tree_of_numbers(size);
    print_left_pointing_tree_of_numbers(size);
    print_both_sides_pointing_tree_of_numbers(size);
    //
    printf ( "\n\n\n" );
    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */



/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  print_right_pointing_tree
 *  Description:  
 * =====================================================================================
 */
    void
print_right_pointing_tree ( int size )
{
    for ( int i=0; i<=size; i++)
    {
        printf ( "\n" );
        for ( int j=1; j<=i; j++)
        {
            printf ( "*\t" );
        }
    }
}		/* -----  end of function print_right_pointing_tree  ----- */



/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  print_right_pointing_tree_of_numbers
 *  Description:  
 * =====================================================================================
 */
    void
print_right_pointing_tree_of_numbers ( int size )
{
    for ( int i=0; i<=size; i++)
    {
        printf ( "\n" );
        for ( int j=1; j<=i; j++)
        {
            printf ( "%d\t",j );
        }
    }
}		/* -----  end of function print_right_pointing_tree_of_numbers  ----- */

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  print_left_pointing_tree
 *  Description:  
 * =====================================================================================
 */
    void
print_left_pointing_tree ( int size )
{
    for ( int i=0; i<=size; i++)
    {
        printf ( "\n" );
        for ( int j=1; j<=size; j++)
        {
            if (j<=size-i)
                printf ( "\t" );
            else
                printf ( "*\t" );
        }
    }
}		/* -----  end of function print_left_pointing_tree  ----- */


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  print_left_pointing_tree_of_numbers
 *  Description:  
 * =====================================================================================
 */
    void
print_left_pointing_tree_of_numbers ( int size )
{
    for ( int i=0; i<=size; i++)
    {
        printf ( "\n" );
        for ( int j=1; j<=size; j++)
        {
            if (j<=size-i)
                printf ( "\t" );
            else
                printf ( "%d\t",size-j+1 );
        }
    }
}		/* -----  end of function print_left_pointing_tree_of_numbers  ----- */


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  print_both_sides_pointing_tree
 *  Description:  
 * =====================================================================================
 */
    void
print_both_sides_pointing_tree ( int size )
{
    int rowsize = size*2-1;
    for ( int i=0; i<=size; i++)
    {
        printf ( "\n" );
        for ( int j=1; j<=rowsize; j++)
        {
            if (j > size-i && j < size+i)
                printf ( "*\t" );
            else
                printf ( "\t" );
        }
    }
    
}		/* -----  end of function print_both_sides_pointing_tree  ----- */


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  print_both_sides_pointing_tree_of_numbers
 *  Description:  
 * =====================================================================================
 */
    void
print_both_sides_pointing_tree_of_numbers ( int size )
{
    int rowsize = size*2-1;
    for ( int i=0; i<=size; i++)
    {
        printf ( "\n" );
        for ( int j=1; j<=rowsize; j++)
        {
            if (j > size-i && j < size+i)
                printf ( "%d\t",j-size+i );
            else
                printf ( "\t" );
        }
    }

}		/* -----  end of function print_both_sides_pointing_tree_of_numbers  ----- */
