	.file	"printpatterns.c"
	.section	.rodata
	.align 8
.LC0:
	.string	"Enter the length of the tree required"
.LC1:
	.string	"%d"
.LC2:
	.string	"\n\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movl	%edi, -20(%rbp)
	movq	%rsi, -32(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	$0, -12(%rbp)
	movl	$.LC0, %edi
	call	puts
	leaq	-12(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC1, %edi
	movl	$0, %eax
	call	__isoc99_scanf
	movl	-12(%rbp), %eax
	movl	%eax, %edi
	call	print_right_pointing_tree
	movl	-12(%rbp), %eax
	movl	%eax, %edi
	call	print_left_pointing_tree
	movl	-12(%rbp), %eax
	movl	%eax, %edi
	call	print_both_sides_pointing_tree
	movl	-12(%rbp), %eax
	movl	%eax, %edi
	call	print_right_pointing_tree_of_numbers
	movl	-12(%rbp), %eax
	movl	%eax, %edi
	call	print_left_pointing_tree_of_numbers
	movl	-12(%rbp), %eax
	movl	%eax, %edi
	call	print_both_sides_pointing_tree_of_numbers
	movl	$.LC2, %edi
	call	puts
	movl	$0, %eax
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	je	.L3
	call	__stack_chk_fail
.L3:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2:
	.size	main, .-main
	.section	.rodata
.LC3:
	.string	"*\t"
	.text
	.globl	print_right_pointing_tree
	.type	print_right_pointing_tree, @function
print_right_pointing_tree:
.LFB3:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movl	%edi, -20(%rbp)
	movl	$0, -8(%rbp)
	jmp	.L5
.L8:
	movl	$10, %edi
	call	putchar
	movl	$1, -4(%rbp)
	jmp	.L6
.L7:
	movl	$.LC3, %edi
	movl	$0, %eax
	call	printf
	addl	$1, -4(%rbp)
.L6:
	movl	-4(%rbp), %eax
	cmpl	-8(%rbp), %eax
	jle	.L7
	addl	$1, -8(%rbp)
.L5:
	movl	-8(%rbp), %eax
	cmpl	-20(%rbp), %eax
	jle	.L8
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3:
	.size	print_right_pointing_tree, .-print_right_pointing_tree
	.section	.rodata
.LC4:
	.string	"%d\t"
	.text
	.globl	print_right_pointing_tree_of_numbers
	.type	print_right_pointing_tree_of_numbers, @function
print_right_pointing_tree_of_numbers:
.LFB4:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movl	%edi, -20(%rbp)
	movl	$0, -8(%rbp)
	jmp	.L10
.L13:
	movl	$10, %edi
	call	putchar
	movl	$1, -4(%rbp)
	jmp	.L11
.L12:
	movl	-4(%rbp), %eax
	movl	%eax, %esi
	movl	$.LC4, %edi
	movl	$0, %eax
	call	printf
	addl	$1, -4(%rbp)
.L11:
	movl	-4(%rbp), %eax
	cmpl	-8(%rbp), %eax
	jle	.L12
	addl	$1, -8(%rbp)
.L10:
	movl	-8(%rbp), %eax
	cmpl	-20(%rbp), %eax
	jle	.L13
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4:
	.size	print_right_pointing_tree_of_numbers, .-print_right_pointing_tree_of_numbers
	.globl	print_left_pointing_tree
	.type	print_left_pointing_tree, @function
print_left_pointing_tree:
.LFB5:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movl	%edi, -20(%rbp)
	movl	$0, -8(%rbp)
	jmp	.L15
.L20:
	movl	$10, %edi
	call	putchar
	movl	$1, -4(%rbp)
	jmp	.L16
.L19:
	movl	-20(%rbp), %eax
	subl	-8(%rbp), %eax
	cmpl	-4(%rbp), %eax
	jl	.L17
	movl	$9, %edi
	call	putchar
	jmp	.L18
.L17:
	movl	$.LC3, %edi
	movl	$0, %eax
	call	printf
.L18:
	addl	$1, -4(%rbp)
.L16:
	movl	-4(%rbp), %eax
	cmpl	-20(%rbp), %eax
	jle	.L19
	addl	$1, -8(%rbp)
.L15:
	movl	-8(%rbp), %eax
	cmpl	-20(%rbp), %eax
	jle	.L20
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5:
	.size	print_left_pointing_tree, .-print_left_pointing_tree
	.globl	print_left_pointing_tree_of_numbers
	.type	print_left_pointing_tree_of_numbers, @function
print_left_pointing_tree_of_numbers:
.LFB6:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movl	%edi, -20(%rbp)
	movl	$0, -8(%rbp)
	jmp	.L22
.L27:
	movl	$10, %edi
	call	putchar
	movl	$1, -4(%rbp)
	jmp	.L23
.L26:
	movl	-20(%rbp), %eax
	subl	-8(%rbp), %eax
	cmpl	-4(%rbp), %eax
	jl	.L24
	movl	$9, %edi
	call	putchar
	jmp	.L25
.L24:
	movl	-20(%rbp), %eax
	subl	-4(%rbp), %eax
	addl	$1, %eax
	movl	%eax, %esi
	movl	$.LC4, %edi
	movl	$0, %eax
	call	printf
.L25:
	addl	$1, -4(%rbp)
.L23:
	movl	-4(%rbp), %eax
	cmpl	-20(%rbp), %eax
	jle	.L26
	addl	$1, -8(%rbp)
.L22:
	movl	-8(%rbp), %eax
	cmpl	-20(%rbp), %eax
	jle	.L27
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6:
	.size	print_left_pointing_tree_of_numbers, .-print_left_pointing_tree_of_numbers
	.globl	print_both_sides_pointing_tree
	.type	print_both_sides_pointing_tree, @function
print_both_sides_pointing_tree:
.LFB7:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movl	%edi, -20(%rbp)
	movl	-20(%rbp), %eax
	addl	%eax, %eax
	subl	$1, %eax
	movl	%eax, -4(%rbp)
	movl	$0, -12(%rbp)
	jmp	.L29
.L34:
	movl	$10, %edi
	call	putchar
	movl	$1, -8(%rbp)
	jmp	.L30
.L33:
	movl	-20(%rbp), %eax
	subl	-12(%rbp), %eax
	cmpl	-8(%rbp), %eax
	jge	.L31
	movl	-20(%rbp), %edx
	movl	-12(%rbp), %eax
	addl	%edx, %eax
	cmpl	-8(%rbp), %eax
	jle	.L31
	movl	$.LC3, %edi
	movl	$0, %eax
	call	printf
	jmp	.L32
.L31:
	movl	$9, %edi
	call	putchar
.L32:
	addl	$1, -8(%rbp)
.L30:
	movl	-8(%rbp), %eax
	cmpl	-4(%rbp), %eax
	jle	.L33
	addl	$1, -12(%rbp)
.L29:
	movl	-12(%rbp), %eax
	cmpl	-20(%rbp), %eax
	jle	.L34
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7:
	.size	print_both_sides_pointing_tree, .-print_both_sides_pointing_tree
	.globl	print_both_sides_pointing_tree_of_numbers
	.type	print_both_sides_pointing_tree_of_numbers, @function
print_both_sides_pointing_tree_of_numbers:
.LFB8:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movl	%edi, -20(%rbp)
	movl	-20(%rbp), %eax
	addl	%eax, %eax
	subl	$1, %eax
	movl	%eax, -4(%rbp)
	movl	$0, -12(%rbp)
	jmp	.L36
.L41:
	movl	$10, %edi
	call	putchar
	movl	$1, -8(%rbp)
	jmp	.L37
.L40:
	movl	-20(%rbp), %eax
	subl	-12(%rbp), %eax
	cmpl	-8(%rbp), %eax
	jge	.L38
	movl	-20(%rbp), %edx
	movl	-12(%rbp), %eax
	addl	%edx, %eax
	cmpl	-8(%rbp), %eax
	jle	.L38
	movl	-8(%rbp), %eax
	subl	-20(%rbp), %eax
	movl	%eax, %edx
	movl	-12(%rbp), %eax
	addl	%edx, %eax
	movl	%eax, %esi
	movl	$.LC4, %edi
	movl	$0, %eax
	call	printf
	jmp	.L39
.L38:
	movl	$9, %edi
	call	putchar
.L39:
	addl	$1, -8(%rbp)
.L37:
	movl	-8(%rbp), %eax
	cmpl	-4(%rbp), %eax
	jle	.L40
	addl	$1, -12(%rbp)
.L36:
	movl	-12(%rbp), %eax
	cmpl	-20(%rbp), %eax
	jle	.L41
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8:
	.size	print_both_sides_pointing_tree_of_numbers, .-print_both_sides_pointing_tree_of_numbers
	.ident	"GCC: (Ubuntu 5.4.0-6ubuntu1~16.04.4) 5.4.0 20160609"
	.section	.note.GNU-stack,"",@progbits
