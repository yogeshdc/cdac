/*
 * =====================================================================================
 *
 *       Filename:  ramanujan.c
 *
 *    Description:  Code to find Ramanujan number
 *
 *        Version:  1.0
 *        Created:  Thursday 24 August 2017 08:29:57  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include	<stdlib.h>
#include	<stdio.h>
#include        <math.h>

int find_pair ( int , int );
/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
int main ( int argc, char *argv[] )
{
    for ( int i = 1; i<=200000; i++ ) 
    {
        //We need the max number in either of the pairs to be cuberoot of i
        int max_number = floor(cbrt(i));

        int num_pairs = 0;
        int pair_number1 = 0;
        int pair_number2 = 0;
        int pair_number3 = 0;
        int pair_number4 = 0;
        //Find a pair of sum of cubes starting from 1 to the max number

        for (int j = 1; j <= max_number; j++)
        {
            int other_number = find_pair (i, j);
            if (other_number != 0)
            {
                num_pairs +=1;
                if (pair_number1 == 0) 
                {
                    pair_number1 = other_number;
                    pair_number2 = j;
                }
                else
                {
                    pair_number3 = other_number;
                    pair_number4 = j;
                }

                if (num_pairs == 2)
                {
                    printf ("\n\n***The Number %d is a Ramanujan Number***\n",i);
                    printf ("Its factors are %dcube + %d cube\n",pair_number1, pair_number2);
                    printf ("And also %dcube + %d cube\n",pair_number3, pair_number4);
                    break;
                }
            }

        }
    }

    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  find_pair
 *  Description:  
 * =====================================================================================
 */
int find_pair ( int number, int max_cube_root )
{
    int sum = 0;
    int pair1=0, pair2=0;
    for (int i = 1; i < max_cube_root; i++)
    {
        sum = pow(max_cube_root,3)+pow(i,3);
        if (sum>number)
            break;
        else if (sum == number)
        {
            pair1=max_cube_root;
            pair2=i;
            break;            
        }
        else
            continue;
    }
    
    return pair2;
}		/* -----  end of function find_pair  ----- */
