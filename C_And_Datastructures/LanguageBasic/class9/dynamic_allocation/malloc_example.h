/*
 * =====================================================================================
 *
 *       Filename:  malloc_example.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Tuesday 29 August 2017 09:10:06  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */

void accept_input_array (int*, int);
void display_array (int*, int);
