/*
 * =====================================================================================
 *
 *       Filename:  display_array.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Tuesday 29 August 2017 09:11:35  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */

#include <stdio.h>

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  display_array
 *  Description:  
 * =====================================================================================
 */
void display_array ( int* ptr, int size )
{
    for ( int i=0; i<size; i++ )
    {
        printf ( "%d\t", *(ptr+i) );
    }
    printf ( "\n" );

}		/* -----  end of function display_array  ----- */
