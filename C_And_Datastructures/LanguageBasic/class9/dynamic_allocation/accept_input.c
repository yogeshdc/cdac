/*
 * =====================================================================================
 *
 *       Filename:  accept_input.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Tuesday 29 August 2017 09:10:51  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */
#include <stdio.h>

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  accept_input_array
 *  Description:  
 * =====================================================================================
 */
void accept_input_array ( int* ptr, int size )
{
    printf ( "Enter %d numbers\n",size );
    for ( int i=0; i<size; i++ )
    {
        scanf ( "%d", (ptr+i) );
    }
}		/* -----  end of function accept_input_array  ----- */

