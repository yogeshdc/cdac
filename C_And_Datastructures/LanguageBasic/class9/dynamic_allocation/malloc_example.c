/*
 * =====================================================================================
 *
 *       Filename:  malloc_example.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Tuesday 29 August 2017 08:53:32  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include	<stdlib.h>
#include	<stdio.h>
#include        "malloc_example.h"



/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
    int
main ( int argc, char *argv[] )
{

    int size;
    int *ptr = NULL;
    printf ( "Enter the size of array: \n" );
    scanf ( "%d", &size );
    ptr	= (int*) malloc ( sizeof(int) );
    if ( ptr==NULL ) {
        fprintf ( stderr, "\ndynamic memory allocation failed\n" );
        exit (EXIT_FAILURE);
    }

    accept_input_array(ptr, size);
    display_array(ptr, size);

    free ( ptr );
    ptr	= NULL;

    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */


