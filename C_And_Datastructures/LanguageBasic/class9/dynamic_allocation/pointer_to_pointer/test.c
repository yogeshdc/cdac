/*
 * =====================================================================================
 *
 *       Filename:  test.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Tuesday 29 August 2017 09:17:04  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include	<stdlib.h>
#include       <stdio.h>

int** allocate_memory ( int**, int, int );
//void allocate_memory ( int***, int, int );
void accept_matrix_input ( int**, int, int );
void display_matrix ( int**, int, int );
void free_matrix (int**, int);
/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  Denmo code for pointer to pointer (2d array) 
 * =====================================================================================
 */
    int
main ( int argc, char *argv[] )
{
    int **matrix = NULL;
    int row = 3, col = 3;

    matrix=allocate_memory (matrix, row , col);
    //allocate_memory (&matrix, row , col);
    accept_matrix_input (matrix, row, col);
    display_matrix (matrix, row, col);
    free_matrix(matrix, row);
    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  allocate_memory
 *  Description:  
 *                Allocate memory for the array of pointers and
 *                Allocate memory for each coloumn of 2d array
 * =====================================================================================
 */
int** 
 allocate_memory ( int** matrix, int row, int col )
{
    matrix = (int**) malloc ( sizeof(int*) * row);

    for ( int i=0; i<row; i++ )
    {
        matrix[i] = (int*) malloc( sizeof(int) * col );
    }
    return matrix;
}		/* -----  end of function allocate_memory  ----- */


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  accept_matrix_input
 *  Description:  Accept/Assign input to matrix
 * =====================================================================================
 */
void accept_matrix_input ( int** matrix, int row, int col )
{
        for ( int i=0; i<row; i++ )
    {
        for ( int j=0; j<col; j++ )
        {
            printf ( "Enter matrix element %d,%d: \n",i,j );
            scanf ( "%d", &matrix[i][j] );
            //matrix[i][j] = i*10+j;
        }
    }
}		/* -----  end of function accept_matrix_input  ----- */



/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  display_matrix
 *  Description:  Display contents of matrix
 * =====================================================================================
 */
void display_matrix ( int** matrix, int row, int col )
{ 
    for ( int i=0; i<row; i++ )
    {
        printf ( "\n" );
        for ( int j=0; j<col; j++ )
        {
            printf ( "%d\t",matrix[i][j]);
            printf ( "Address is: %p\t",*(matrix+i)+j); //  *(matrix+i)+j = &matrix[i][j]   
        }
    }
}		/* -----  end of function display_matrix  ----- */


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  free_matrix
 *  Description:  
 * =====================================================================================
 */
void free_matrix ( int** matrix, int row )
{
    for ( int i=0; i<row; i++ )
    {
        free(matrix[i]);
    }
    free(matrix);
}		/* -----  end of function free_matrix  ----- */
