/*
 * =====================================================================================
 *
 *       Filename:  pointer_size.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Saturday 26 August 2017 03:41:11  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include	<stdlib.h>
#include	<stdio.h>

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
int main ( int argc, char *argv[] )
{

    printf ( "***************INT*****************************\n" );
    int num = 10;
    int *iptr;
    iptr = &num;
    printf ( "Number value is %d\n",num );
    printf ( "Number value from pointer is %d\n",*iptr );
    printf ( "Address of 'num' is %p\n",&num );
    printf ( "Address of 'num' using pointer is %p\n",iptr );
    printf ( "Size of pointer = %lu\n",sizeof(iptr));
    printf ( "***********************************************\n" );

    printf ( "***************FLOAT***************************\n" );
    float fnum = 10.0;
    float *fptr;
    fptr = &fnum;
    printf ( "Number value is %f\n",fnum );
    printf ( "Number value from pointer is %f\n",*fptr );
    printf ( "Address of 'num' is %p\n",&fnum );
    printf ( "Address of 'num' using pointer is %p\n",fptr );
    printf ( "Size of pointer = %lu\n",sizeof(fptr));
    printf ( "***********************************************\n" );

    printf ( "**************CHAR*****************************\n" );
    char ch = 'a';
    char *cptr;
    cptr = &ch;
    printf ( "Number value is %c\n",ch );
    printf ( "Number value from pointer is %c\n",*cptr );
    printf ( "Address of 'num' is %p\n",&ch );
    printf ( "Address of 'num' using pointer is %p\n",cptr );
    printf ( "Size of pointer = %lu\n",sizeof(cptr));
    printf ( "***********************************************\n" );

    printf ( "**************LONG*****************************\n" );
    long lnum = 10;
    long *lptr;
    lptr = &lnum;
    printf ( "Number value is %ld\n",lnum );
    printf ( "Number value from pointer is %ld\n",*lptr );
    printf ( "Address of 'num' is %p\n",&lnum );
    printf ( "Address of 'num' using pointer is %p\n",lptr );
    printf ( "Size of pointer = %lu\n",sizeof(lptr));
    printf ( "***********************************************\n" );

    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */
