/*
 * =====================================================================================
 *
 *       Filename:  swap_main.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Saturday 26 August 2017 02:31:58  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include	<stdlib.h>
#include	<stdio.h>
#include        "swap.h"

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
    int
main ( int argc, char *argv[] )
{
    int num1, num2;
    printf ("Enter 2 numbers to swap: ");
    scanf ( "%d %d", &num1, &num2 );
    printf ( "Before Swapping : Num1 = %d and Num2 = %d\n",num1,num2 );
    swap_ (&num1,&num2);
    printf ( "After Swapping  : Num1 = %d and Num2 = %d\n",num1,num2 );
    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */
