/*
 * =====================================================================================
 *
 *       Filename:  arithmatic.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Friday 25 August 2017 07:27:02  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */



// Global Variables
//



// Struct/Union/Enum/Array declaration/Class
//


//Function Declaration
//
void calculator (int, int, int*, int*, long*, float*);
int add (int, int);
int sub(int, int);
long mul(int, int);
float div(int, int);

