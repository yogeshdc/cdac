/*
 * =====================================================================================
 *
 *       Filename:  client.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Saturday 26 August 2017 07:51:30  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */

#include<stdio.h>
#include"header.h"
    int num = 100;
/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  display
 *  Description:  
 * =====================================================================================
 */
void display ()
{
    printf ( "Inside display func\n" );
    printf ( "num = %d\n",num );
    num = 50;
}		/* -----  end of function display  ----- */
