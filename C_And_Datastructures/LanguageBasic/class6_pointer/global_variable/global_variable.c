/*
 * =====================================================================================
 *
 *       Filename:  global_variable.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Saturday 26 August 2017 07:45:42  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include	<stdlib.h>
#include	<stdio.h>

void display ();


int num = 100;


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
    int
main ( int argc, char *argv[] )
{
    printf ( "Inside main\n" );
    printf ( "num = %d\n",num );
    display();
    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */



/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  display
 *  Description:  
 * =====================================================================================
 */
    void
display (  )
{
    printf ( "Inside display func\n" );
    printf ( "num = %d\n",num );
}		/* -----  end of function display  ----- */
