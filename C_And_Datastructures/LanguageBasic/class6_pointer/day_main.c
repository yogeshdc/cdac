/*
 * =====================================================================================
 *
 *       Filename:  day_main.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Saturday 26 August 2017 06:03:11  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include "day.h"

int main()
{
    int day, month, year;
    //date(&day,&month,&year);
    //printf ( "Date is %d//%d//%d\n",day,month,year );

    printf ("Date is %s \n",date());
    return 0;
}
