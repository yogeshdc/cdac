/*
 * =====================================================================================
 *
 *       Filename:  storage_class.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Saturday 26 August 2017 07:24:09  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include	<stdlib.h>
#include	<stdio.h>

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */


void static_display ()
{
    static int i = 0;
    i++;
    printf ( "%d\n",i );
}



void display ()
{
    int j = 0;
    j++;
    printf ( "%d\n",j );
}


int main ( int argc, char *argv[] )
{
    auto int num = 10;
    //register int num = 10;
    //static int num = 10;
    printf ( "Inside main\n" );
    printf ( "Value of num = %d\n", num );
    //printf ( "Address of num = %lu\n", &num ); //This will give compile time error if num is register type storage class
    {
        auto int num = 100;
        //register int num = 100;
        //static int num = 100;
        printf ( "Inside Block\n" );
        printf ( "Value of num = %d\n", num );
        printf ( "Exiting Block\n" );
    }

    printf ( "Inside main again\n" );
    printf ( "Value of num = %d\n", num );

    printf ( "\n\nwith auto integer\n" );
    display();
    display();
    display();


    printf ( "\n\nwith static integer\n" );
    static_display();
    static_display();
    static_display();
    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */
