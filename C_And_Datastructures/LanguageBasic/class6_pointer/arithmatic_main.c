/*
 * =====================================================================================
 *
 *       Filename:  main.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Friday 25 August 2017 07:28:38  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include        <stdio.h>
#include        "arithmatic.h"

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
int main ( int argc, char *argv[] )
{
    int num1, num2;

    printf ( "Enter first number:\n" );
    scanf ( "%d", &num1 );
    printf ( "Enter second number:\n" );
    scanf ( "%d", &num2 );
   
    int addition_result, subtraction_result;
    long multiplication_result;
    float division_result;
    calculator (num1, num2, &addition_result, &subtraction_result, &multiplication_result, &division_result);
    printf ("Sum of %d and %d is %d\n",num1, num2, addition_result );
    printf ("Difference of %d and %d is %d\n",num1, num2,subtraction_result );
    printf ("Product of %d and %d is %ld\n",num1, num2, multiplication_result);
    printf ("Division of %d and %d is %f\n",num1, num2, division_result);
    
    return 0;
}				/* ----------  end of function main  ---------- */
