/*
 * =====================================================================================
 *
 *       Filename:  date2.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Saturday 26 August 2017 06:35:20  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include <stdio.h>
#include <stdlib.h>

char result[] = "          ";
char* date()
{
    int day, month, year;
    printf("Enter day month and year respectively:\n");
    scanf("%d %d %d",&day,&month,&year);
    snprintf(result, sizeof(result), "%d/%d/%d", day, month, year);
    return result;
}
