/*
 * =====================================================================================
 *
 *       Filename:  arithmatic.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Friday 25 August 2017 07:28:30  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */
#include        <stdio.h>
#include        "arithmatic.h"


int add(int num1, int num2)
{
    return (num1+num2);
}
int sub(int num1, int num2)
{
    return (num1-num2);
}
long mul(int num1, int num2)
{
    return (num1*num2);
}
float div(int num1, int num2)
{
    return (num1/num2);
}


void calculator (int num1, int num2, int* addition_result, int* subtraction_result, long* multiplication_result, float* division_result)
{
    *addition_result = add(num1, num2);
    *subtraction_result = sub(num1,num2);
    *multiplication_result = mul(num1,num2);
    *division_result = div(num1,num2);
}
