/*
 * =====================================================================================
 *
 *       Filename:  day.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Saturday 26 August 2017 05:58:03  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */

#include <stdio.h>
#include <stdlib.h>

void date(int* day, int* month, int* year)
{
    printf("Enter day month and year respectively:\n");
    scanf("%d %d %d",day,month,year);
}
