/*
 * =====================================================================================
 *
 *       Filename:  Max_of_three.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Friday 25 August 2017 08:04:56  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */
int max_of_three(int ,int ,int);
