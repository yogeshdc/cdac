/*
 * =====================================================================================
 *
 *       Filename:  swap.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Saturday 26 August 2017 02:30:26  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */
#include <stdlib.h>

void swap_ (int *num1, int *num2)
{
    
    int temp;
    temp = *num1;
    *num1 = *num2;
    *num2 = temp;
}
