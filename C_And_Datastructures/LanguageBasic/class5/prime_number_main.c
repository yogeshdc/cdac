/*
 * =====================================================================================
 *
 *       Filename:  prime_number_main.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Friday 25 August 2017 08:43:01  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */



#include <stdio.h>
#include "prime_number.h"

int main()
{
    int num;
    printf("Enter number:");
    scanf("%d",&num);
    prime_number(num);
    return 0;
}

