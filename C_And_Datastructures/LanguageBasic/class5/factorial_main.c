/*
 * =====================================================================================
 *
 *       Filename:  factorial_main.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Friday 25 August 2017 09:25:11  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */

#include <stdio.h>
#include "factorial.h"



int main()
{
    int num;
    printf("Enter number:");
    scanf("%d",&num);
    factorial(num);
    return 0;
}

