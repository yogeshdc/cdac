/*
 * =====================================================================================
 *
 *       Filename:  arithmatic.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Friday 25 August 2017 07:28:30  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */
#include        <stdio.h>
#include        "arithmatic.h"

void add(int num1, int num2)
{
    printf ("Sum of %d and %d is %d\n",num1, num2, num1+num2);
}
void sub(int num1, int num2)
{
    printf ("Difference of %d and %d is %d\n",num1, num2, num1-num2);
}
void mul(int num1, int num2)
{
    printf ("Product of %d and %d is %d\n",num1, num2, num1*num2);
}
void div(int num1, int num2)
{
    printf ("Division of %d and %d is %d\n",num1, num2, num1/num2);
}
