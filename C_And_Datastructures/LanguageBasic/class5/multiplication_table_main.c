/*
 * =====================================================================================
 *
 *       Filename:  multiplication_table_main.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Saturday 26 August 2017 02:14:04  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */



#include <stdio.h>
#include "multiplication_table.h"

int main()
{
    int num;
    printf("Enter number for multiplication table:");
    scanf("%d",&num);
    display_multiplication_table(num);
    return 0;
}

