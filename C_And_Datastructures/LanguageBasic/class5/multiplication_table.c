/*
 * =====================================================================================
 *
 *       Filename:  multiplication_table.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Saturday 26 August 2017 10:41:32  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */



#include	<stdlib.h>
#include	<stdio.h>


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  For loop exaples
 * =====================================================================================
 */
int display_multiplication_table ( int number )
{

    for ( int i=1; i<=12; i++ ) 
    {
        printf ( "\t\t %7d * %3d = %15d \n",number,i,number*i );
    }

    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */
