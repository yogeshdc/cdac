/*
 * =====================================================================================
 *
 *       Filename:  absolute_main.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Friday 25 August 2017 08:54:26  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */



#include <stdio.h>
#include "absolute.h"

int main()
{
    int num;
    printf("Enter number:");
    scanf("%d",&num);
    num=absolute(num);
    printf("\n%d\n",num);
    return 0;
}

