/*
 * =====================================================================================
 *
 *       Filename:  reverse.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Friday 25 August 2017 08:12:11  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */
int reverse_of_no(int);
