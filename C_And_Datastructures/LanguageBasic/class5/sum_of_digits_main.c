/*
 * =====================================================================================
 *
 *       Filename:  sum_of_digits_main.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Friday 25 August 2017 07:59:51  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */

#include <stdio.h>
#include "sum_of_digits.h"

int main()
{
    int num;
    printf("Enter number:");
    scanf("%d",&num);
    printf ( "Sum of digits of %d is %d\n",num,sum_of_digits(num) );
    return 0;
}
