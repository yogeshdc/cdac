/*
 * =====================================================================================
 *
 *       Filename:  changecase_main.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Friday 25 August 2017 08:29:53  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */



#include <stdio.h>
#include "changecase.h"

int main()
{
    char ch;
    printf("Enter character:");
    scanf("%c",&ch);
    changecase(ch);
    return 0;
}

