/*
 * =====================================================================================
 *
 *       Filename:  fibonacci.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Friday 25 August 2017 09:28:48  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */

#include <stdio.h>


int fibonacci(int num)
{
    
    int a= 0;
    int b = 1;
    int c=0;
    if (num==1)
        printf ( "1\n" );
    else
    {

    
        printf ( "1" );
        for (int i=1; i<num; i++)
        {
            c=a+b; 
            a=b;
            b=c;
            printf(",%d",c);
        }
    }
        return 0;
}
