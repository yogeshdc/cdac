/*
 * =====================================================================================
 *
 *       Filename:  factorial.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Friday 25 August 2017 09:20:35  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */

#include <stdio.h>
long factorial(int num)
{
    long res=1;
    for(int i=1;i<=num;i++)
    {
        res*=i;
    }
    printf("%d factorial is %ld",num,res);
    return res;
}
