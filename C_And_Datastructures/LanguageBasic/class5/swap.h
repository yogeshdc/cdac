/*
 * =====================================================================================
 *
 *       Filename:  swap.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Saturday 26 August 2017 02:29:53  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


void swap_ (int* num1, int* num2);
