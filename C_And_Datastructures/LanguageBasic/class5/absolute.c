/*
 * =====================================================================================
 *
 *       Filename:  absolute.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Friday 25 August 2017 08:52:40  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */

#include <stdio.h>
int absolute(int num)
{
    if(num>=0)
        return num;
    else
        return (-1*num);

}
