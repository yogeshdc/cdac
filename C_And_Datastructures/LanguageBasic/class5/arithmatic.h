/*
 * =====================================================================================
 *
 *       Filename:  arithmatic.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Friday 25 August 2017 07:27:02  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */



// Global Variables
//



// Struct/Union/Enum/Array declaration/Class
//


//Function Declaration
//

void add (int, int);
void sub(int, int);
void mul(int, int);
void div(int, int);

