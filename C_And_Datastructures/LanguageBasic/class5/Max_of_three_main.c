/*
 * =====================================================================================
 *
 *       Filename:  Max_of_three_main.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Friday 25 August 2017 08:08:26  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include <stdio.h>
#include "Max_of_three.h"

int main()
{
    int num1,num2,num3;
    printf("Enter 3 numbers:");
    scanf("%d%d%d",&num1,&num2,&num3);
    max_of_three(num1,num2,num3);
    return 0;
}
