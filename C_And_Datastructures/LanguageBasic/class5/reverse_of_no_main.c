/*
 * =====================================================================================
 *
 *       Filename:  reverse_of_no_main.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Friday 25 August 2017 08:15:26  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include <stdio.h>
#include "reverse.h"

int main()
{
    int num;
    printf("Enter number:");
    scanf("%d",&num);
    reverse_of_no(num);
    return 0;
}

