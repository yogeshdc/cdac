/*
 * =====================================================================================
 *
 *       Filename:  armstrong_main.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Friday 25 August 2017 08:47:51  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */



#include <stdio.h>
#include "armstrong.h"

int main()
{
    int num;
    printf("Enter number:");
    scanf("%d",&num);
    armstrong(num);
    return 0;
}

