/*
 * =====================================================================================
 *
 *       Filename:  square_root_main.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Friday 25 August 2017 09:14:59  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */



#include <stdio.h>
#include "square_root.h"

int main()
{
    int num;
    printf("Enter number:");
    scanf("%d",&num);
    float res = square_root(num);
    return 0;
}

