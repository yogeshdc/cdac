/*
 * =====================================================================================
 *
 *       Filename:  main.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Friday 25 August 2017 07:28:38  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include        <stdio.h>
#include        "arithmatic.h"

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
int main ( int argc, char *argv[] )
{
    int num1, num2, choice;
    printf ( "\t\t\t\t **** MENU ****\n" );
    printf ( "1. Addition\n" );
    printf ( "2. Subtraction\n" );
    printf ( "3. Multiplication\n" );
    printf ( "4. Division\n" );
    printf ( "Enter your choice\n" );
    scanf ( "%d", &choice );

    printf ( "Enter first number:\n" );
    scanf ( "%d", &num1 );
    printf ( "Enter second number:\n" );
    scanf ( "%d", &num2 );

    
    
    switch ( choice ) {
        case 1:	
            add (num1,num2); 
            break;

        case 2:	
            sub (num1,num2); 
            break;

        case 3:	
            mul (num1,num2); 
            break;

        case 4:	
            div (num1,num2); 
            break;
       
        default:	
            printf ( "Invalid input received\n" );
            break;
    }				/* -----  end switch  ----- */
    
    
    
    
    return 0;
}				/* ----------  end of function main  ---------- */
