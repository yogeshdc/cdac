/*
 * =====================================================================================
 *
 *       Filename:  fibonacci_main.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Friday 25 August 2017 09:37:29  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */




#include <stdio.h>
#include "fibonacci.h"

int main()
{
    int num;
    printf("Enter number:");
    scanf("%d",&num);
    fibonacci(num);
    return 0;
}

