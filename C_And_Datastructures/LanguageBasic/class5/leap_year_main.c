/*
 * =====================================================================================
 *
 *       Filename:  leap_year_main.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Friday 25 August 2017 08:22:34  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */



#include <stdio.h>
#include "leap_year.h"

int main()
{
    int num;
    printf("Enter year:");
    scanf("%d",&num);
    leap_year(num);
    return 0;
}


