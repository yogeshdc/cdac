/*
 * =====================================================================================
 *
 *       Filename:  prime_number.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Friday 25 August 2017 08:34:05  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */

#include <stdio.h>
#include <stdlib.h>

void prime_number(int num)
{
    int count=0;
    for(int i=1;i<=num;i++)
    {
        
        if(num%i==0)
            count++;
        if(count>2)
        {
            printf ( "Number is not prime\n" );
            break;
        }
    }
    if(count<=2)
        printf ( "Number is prime\n" );
}
