/*
 * =====================================================================================
 *
 *       Filename:  power_main.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Friday 25 August 2017 09:06:52  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include <stdio.h>
#include "power.h"

int main()
{
    float num;
    int exp;
    printf("Enter number and exponent:");
    scanf("%f%d",&num,&exp);
    power(num,exp);
    return 0;
}

