/*
 * =====================================================================================
 *
 *       Filename:  changecase.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Friday 25 August 2017 08:27:17  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */
void changecase (char );
