/*
 * =====================================================================================
 *
 *       Filename:  square_root.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Friday 25 August 2017 09:12:42  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */

#include <stdio.h>
#include <math.h>
float  square_root(int num)
{
    float res = 0.0;
    for (int i=1; i<=num; i++)
    {
        if (i*i == num)
        {
            res = i;
            break;
        }
        else if (i*i > num)
        {

            res = i-1;
            break;
        }
        else
            continue;
    }

    printf("integer square root of %d is about %d\n",num,(int)res);
    return res;
}
