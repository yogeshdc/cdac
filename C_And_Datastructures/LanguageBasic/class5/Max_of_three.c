/*
 * =====================================================================================
 *
 *       Filename:  greatest.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Tuesday 22 August 2017 06:47:03  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include	<stdio.h>

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
	
int max_of_three(int a,int b,int c)
{
	int d;
	if(a>b&&a>c)
	{
		d=a;
	}
	else if(b>a&&b>c)
	{
		d=b;
	}
	else
	{
		d=c;
	}
	printf("The greatest no is %d\n",d);
	return d;	
}

				/* ----------  end of function main  ---------- */
