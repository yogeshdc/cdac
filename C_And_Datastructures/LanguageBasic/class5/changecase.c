/*
 * =====================================================================================
 *
 *       Filename:  changecase.c
 *
 *    Description:  Change Case
 *
 *        Version:  1.0
 *        Created:  Tuesday 22 August 2017 07:04:08  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include	<stdio.h>
#include	<stdlib.h>

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
void changecase (char ch )
{
	
	if ( (int)ch>=65 && (int)ch <=90)
		printf("%c\n",ch+32);
	else if ((int)ch>=97 && (int)ch <=122)
		printf("%c\n",ch-32);
		
	
}				/* ----------  end of function main  ---------- */
