/*
 * =====================================================================================
 *
 *       Filename:  power.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Friday 25 August 2017 08:59:16  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */

#include <stdio.h>

double power(float num,int exp)
{
    double result=1;
    for(int i=1;i<=exp;i++)
    {
        result*=num;
    }
    printf("%f power %d = %lf",num,exp,result);
    return result;
}
