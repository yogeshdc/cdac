#include <stdio.h>

int main()
{
	printf("Sizeof char is %ld\n",sizeof(char));
	printf("Sizeof short Int is %ld\n",sizeof(short int));
	printf("Sizeof Int is %ld\n",sizeof(int));
	printf("Sizeof float is %ld\n",sizeof(float));
	printf("Sizeof long is %ld\n",sizeof(long));
	printf("Sizeof double is %ld\n",sizeof(double));
	return 0;
}
