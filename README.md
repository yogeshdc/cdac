# README #

This is a repo with a set of practice codes ised in the CDAC IoT course. The [older repo](https://bitbucket.org/yogeshdc/c_and_ds]) is no longer maintained.

### What is this repository for? ###

* Quick summary
* Version 2017.09.08

### How do I get set up? ###

* Everything is tested only on UBUNTU 16.04
* For C - gcc
* DBMS - mysql-server
* Python - 3.6.1 (use ANACONDA for easier setup)


### Contribution guidelines ###

* Kindly create pull requests if you find any bugs in the code


### Who do I talk to? ###

* Email cyogesh (at) yandex (dot) com
