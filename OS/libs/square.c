/*
 * =====================================================================================
 *
 *       Filename:  square.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Monday 11 September 2017 06:16:19  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  square
 *  Description:  
 * =====================================================================================
 */
int square ( int x )
{
    return x*x;
}		/* -----  end of function square  ----- */
