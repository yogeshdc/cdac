/*
 * =====================================================================================
 *
 *       Filename:  simple_fork.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Tuesday 29 August 2017 05:48:36  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include	<stdlib.h>
#include	<stdio.h>
#include	<sys/types.h>
#include	<sys/wait.h>
#include	<unistd.h>

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
    int
main ( int argc, char *argv[] )
{
    int ret;
    int max = 2;
    int status, options,w;
    int child_max = 4;
    printf ( "Welcome - My pid is %d\n", getpid() );
    printf ( "----------------Starting Fork-------------\n" );
    if (ret = fork() == 0)
    {
        printf ( "I am child with PID = %d and PPID = %d\n",getpid(), getppid() );

        for ( int i=0; i<child_max ;i++ )
        {
            printf ( "I am child and i = %d\n",i );
            printf ( "Child going off to sleep\n" );
            printf ( "child pid =%d; child ppid = %d\n",getpid(), getppid() );
            sleep(1);
        }
        //Introducing exceptions
        int a,b,c=0;
        a = b/c;
        exit(0);
    }
    else if (ret < 0)
    {
            perror ("Fork Error");
            exit(-1);
    }
    else
    {
            printf ( "My ret value was %d\n",ret );
            printf ( "I am Original parent with PID = %d and PPID = %d\n",getpid(), getppid() );
            for ( int i=0; i<max ;i++ )
            {
                printf ( "I am parent and i = %d\n",i );
                printf ( "Parent going off to sleep\n" );
                sleep(1);
            }
            do
            {
                w =  waitpid(-1, &status, WUNTRACED | WCONTINUED);

                if (w == -1) 
                {
                    perror("waitpid");
                    exit(EXIT_FAILURE);
                }
    
                if (WIFEXITED(status)) 
                {
                    printf("exited, status=%d\n", WEXITSTATUS(status));
                } 
                else if (WIFSIGNALED(status)) 
                {
                    printf("killed by signal %d\n", WTERMSIG(status));
                } else if (WIFSTOPPED(status)) 
                {
                    printf("stopped by signal %d\n", WSTOPSIG(status));
                } else if (WIFCONTINUED(status)) 
                {
                    printf("continued\n");
                }
           } while (!WIFEXITED(status) && !WIFSIGNALED(status));

           printf ( "Thank you...\n" );
    }
    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */
