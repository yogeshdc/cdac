/*
 * =====================================================================================
 *
 *       Filename:  demo.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Monday 28 August 2017 05:48:36  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include	<stdlib.h>
#include	<unistd.h>
//In nlinux system call wrappers are prototyped in unistd.h

#include        <stdio.h>
#include        <sys/types.h>
#include        <sys/stat.h>
#include        <fcntl.h>

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
    int
main ( int argc, char *argv[] )
{
    int fd;
    fd = open("sample.txt", O_WRONLY|O_APPEND|O_CREAT,0666);
    if (fd<0)
    {
        perror("open failed");
        exit(1);
    }

    char str[] = "Hello World!!!";
    int nbytes, len=sizeof(str);
    nbytes = write(fd,str,len);
    if(nbytes<0)
    {
        perror("write");
        exit(2);
    }

    close(fd);

    printf ( "\n\n\n" );
    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */
