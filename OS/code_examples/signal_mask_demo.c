/*
 * =====================================================================================
 *
 *       Filename:  signal_mask_demo.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Friday 01 September 2017 05:49:03  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include	<stdlib.h>
#include	<stdio.h>
#include	<signal.h>

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
int main ( int argc, char *argv[] )
{
    sigset_t mask, old;
    sigemptyset (&mask);
    sigaddset(&mask, SIGINT);
    sigaddset(&mask, SIGHUP);
    sigprocmask(SIG_SETMASK, &mask, &old);
    while(1);
    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */
