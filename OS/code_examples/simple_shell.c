/*
 * =====================================================================================
 *
 *       Filename:  simple_fork.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Tuesday 29 August 2017 05:48:36  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include	<stdlib.h>
#include	<stdio.h>
#include	<sys/types.h>
#include	<sys/wait.h>
#include	<unistd.h>

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
int main ( int argc, char *argv[] )
{
    int ret;
    int status;
    char command[50];

    printf ( "*************Welcome to simple shell****************\n" );
    printf ( "***This will execute simple programs in $PATH*******\n" );
    printf ( "simple_shell> " );
    scanf ( "%s", command );
    ret = fork();

    while(1)
    {
        if (ret == 0)
        {
            //printf ( "%s\n",command );
            int k = execlp(command, command,NULL);
            if (k<0)
            {
                perror("execlp failed");
                exit (EXIT_FAILURE);
            }
            exit(EXIT_SUCCESS);
        }
        else if (ret < 0)
        {
            perror ("Fork Error");
            exit(EXIT_FAILURE);
        }
        else
        {
            do
            {
                int w =  waitpid(-1, &status, WUNTRACED | WCONTINUED);

                if (w == -1) 
                {
                    perror("waitpid");
                    exit(EXIT_FAILURE);
                }
                if (WIFEXITED(status)) 
                {
                    printf("exited, status=%d\n", WEXITSTATUS(status));
                } 
                else if (WIFSIGNALED(status)) 
                {
                    printf("killed by signal %d\n", WTERMSIG(status));
                } 
                else if (WIFSTOPPED(status)) 
                {
                    printf("stopped by signal %d\n", WSTOPSIG(status));
                } 
                else if (WIFCONTINUED(status)) 
                {
                    printf("continued\n");
                }
           } while (!WIFEXITED(status) && !WIFSIGNALED(status));

            printf ( "simple_shell> " );
            scanf ( "%s", command );
            //if (command == "exit" || command == "quit")
            //{
            //    return EXIT_SUCCESS;
            //}
            ret = fork();
       }
    }
    return EXIT_SUCCESS;
}	/* ----------  end of function main  ---------- */
