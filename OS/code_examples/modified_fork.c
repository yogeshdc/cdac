/*
 * =====================================================================================
 *
 *       Filename:  simple_fork.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Tuesday 29 August 2017 05:48:36  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include	<stdlib.h>
#include	<stdio.h>
#include	<sys/types.h>
#include	<unistd.h>

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
    int
main ( int argc, char *argv[] )
{
    int ret;
    int max = 5;
    printf ( "Welcome - My pid is %d\n", getpid() );
    printf ( "----------------Starting Fork-------------\n" );
    if (ret = fork() == 0)
    {
            printf ( "I am child with PID = %d and PPID = %d\n",getpid(), getppid() );

            for ( int i=0; i<max ;i++ )
            {
                printf ( "I am child and i = %d\n",i );
                printf ( "Child going off to sleep\n" );
                sleep(2);
            }

    }
    else if (ret < 0)
    {
            perror ("Fork Error");
            exit(-1);
    }
    else
    {
            printf ( "My ret value was %d\n",ret );
            printf ( "I am Original parent with PID = %d and PPID = %d\n",getpid(), getppid() );
            for ( int i=0; i<max ;i++ )
            {
                printf ( "I am parent and i = %d\n",i );
                printf ( "Parent going off to sleep\n" );
                sleep(2);

            }
            printf ( "Thank you...\n" );
    }
    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */
