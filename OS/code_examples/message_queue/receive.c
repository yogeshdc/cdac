/*
 * =====================================================================================
 *
 *       Filename:  receive.c
 *
 *    Description
 *
 *        Version:  1.0
 *        Created:  Monday 18 September 2017 10:48:46  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include	<stdlib.h>
#include        <stdio.h>
#include        <mqueue.h>
#include        <unistd.h>
#include        <fcntl.h>
#include        <sys/stat.h>
#include        <string.h>

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
int main ( int argc, char *argv[] )
{
    
    mqd_t mqid;
    const int len = 8192;
    int prio = 0, ret = 0;
    mqid = mq_open("/msimple", O_RDONLY, 0666, NULL);
    if (mqid == -1)
    {
        perror("Failed to open msg q in sender");
    }
    
    char buf[8192] = "\0";
    ret = mq_receive(mqid, buf, len, &prio);
    if (ret == -1)
    {
        perror("Failed to open msg q in sender");
    }

    mq_close(mqid);

    printf("The contents of buffer are: %s\n", buf );

    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */
