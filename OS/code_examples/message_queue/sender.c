/*
 * =====================================================================================
 *
 *       Filename:  sender.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Monday 18 September 2017 10:42:13  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include	<stdlib.h>
#include        <stdio.h>
#include        <mqueue.h>
#include        <unistd.h>
#include        <fcntl.h>
#include        <string.h>
#include        <sys/stat.h>

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
int main ( int argc, char *argv[] )
{
    
    mqd_t mqid;
    int prio = 0;
    int ret = 0;
    mqid = mq_open("/msimple", O_WRONLY|O_CREAT, 0666, NULL);
    if (mqid == -1)
    {
        perror("Failed to open msg q in sender");
        exit(EXIT_FAILURE);
    }
    
    char str[50] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890\0";
    ret = mq_send(mqid, str, strlen(str), prio);

    if (ret == -1)
    {
        perror("Failed to send msg q");
        exit(EXIT_FAILURE);
    }

    mq_close(mqid);

    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */
