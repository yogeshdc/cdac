/*
 * =====================================================================================
 *
 *       Filename:  send_rcv_fork.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Monday 18 September 2017 11:12:21  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include        <stdio.h>
#include        <mqueue.h>
#include        <unistd.h>
#include        <fcntl.h>
#include        <string.h>
#include        <sys/stat.h>
#include        <sys/types.h>
#include        <sys/wait.h>
#include	<stdlib.h>

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
int main ( int argc, char *argv[] )
{
    pid_t pid;
    pid = fork();
    int status;
    
    if (pid < 0)
    {
        perror("Error in fork");
    }

    else if (pid == 0)
    {
        printf("In child - receiver\n");
        mqd_t mqid;
        const int len = 8192;
        int prio = 0, ret = 0;
        mqid = mq_open("/msimple", O_RDONLY, 0666, NULL);
        if (mqid == -1)
        {
            perror("Failed to open msg q in sender");
        }
    
        char buf[8192] = "\0";
        ret = mq_receive(mqid, buf, len, &prio);
        if (ret == -1)
        {
            perror("Failed to open msg q in sender");
        }

        mq_close(mqid);

        printf("The contents of buffer are: %s\n", buf );

        return EXIT_SUCCESS;

    }

    else 
    {
        mqd_t mqid;
        int prio = 0;
        int ret = 0;
        mqid = mq_open("/msimple", O_WRONLY|O_CREAT, 0666, NULL);
        if (mqid == -1)
        {
            perror("Failed to open msg q in sender");
            exit(EXIT_FAILURE);
        }
    
        char str[50] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890\0";
        ret = mq_send(mqid, str, strlen(str), prio);

        if (ret == -1)
        {
            perror("Failed to send msg q");
            exit(EXIT_FAILURE);
        }

        mq_close(mqid);

        waitpid(-1, &status, WUNTRACED | WCONTINUED);

        return EXIT_SUCCESS;
    
    }


    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */
