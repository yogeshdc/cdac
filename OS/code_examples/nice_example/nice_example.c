/*
 * =====================================================================================
 *
 *       Filename:  nice_example.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Friday 01 September 2017 02:55:00  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include	<stdlib.h>
#include	<stdio.h>
#include	<unistd.h>

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
int main ( int argc, char *argv[] )
{
    printf ( "Welcome - pid = %d\n",getpid() );
    while(1)
    {
        //Added sleep to prevent system from freezing while changing priority to real time
        sleep(1);
    }
    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */
