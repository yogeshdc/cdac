/*
 * =====================================================================================
 *
 *       Filename:  demo.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Monday 28 August 2017 05:48:36  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include	<stdlib.h>
#include	<unistd.h>
//In nlinux system call wrappers are prototyped in unistd.h

#include        <stdio.h>
#include        <sys/types.h>
#include        <sys/stat.h>
#include        <fcntl.h>

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
    int
main ( int argc, char *argv[] )
{
    int fd;
    fd = open("sample.txt", O_RDONLY);
    if (fd<0)
    {
        perror("open failed");
        exit(1);
    }

    char buf[128];
    int nbytes, maxlen=128;
    nbytes = read(fd,buf,maxlen);
    if(nbytes<0)
    {
        perror("read");
        exit(2);
    }

    buf[nbytes]='\0';
    printf ( "buf=%s\n",buf );

    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */
