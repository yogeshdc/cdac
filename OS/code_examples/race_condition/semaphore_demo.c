/*
 * =====================================================================================
 *
 *       Filename:  simple_threads.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Wednesday 30 August 2017 05:36:16  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */

#include	<stdlib.h>
#include        <stdio.h>
#include        <pthread.h>
#include        <unistd.h>
#include        <semaphore.h>

sem_t sem;
int val = 100;
const int max = 100000;

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  increment
 *  Description:  
 * =====================================================================================
 */
void* increment ( void* pv )
{
    printf ( "pthread A --- Incrementing\n" );
    for ( int i=0; i<max ;i++ )
    {
        //entry section
        sem_wait(&sem);
        //Resource
        val++;
        //exit section
        sem_post(&sem);
    }
    pthread_exit(NULL);
}		/* -----  end of function increment  ----- */



/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  decrement
 *  Description:  
 * =====================================================================================
 */
void* decrement ( void* pv )
{
    printf ( "pthread B ---  Decrementing\n" );
    for ( int i=0; i<max ;i++ )
    {
        //entry section
        sem_wait(&sem);
        //Resource
        val--;
        //exit section
        sem_post(&sem);
    }
    pthread_exit(NULL);
}		/* -----  end of function decrement  ----- */


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
int main ( int argc, char *argv[] )
{

    int pshared = 0; // pshared = 0 indicates that semaphore is shared between threads only
    int ival = 1; // initial value of semaphore

    if (sem_init(&sem, pshared, ival) == -1)
    {
        perror("sem_init failed");
        exit(EXIT_FAILURE);
    }
    pthread_t pt1, pt2;
    pthread_create(&pt1,NULL,increment,NULL);
    pthread_create(&pt2,NULL,decrement,NULL);
    pthread_join(pt1,NULL);
    pthread_join(pt2,NULL);
    sem_destroy(&sem);
    printf ( "Final value of val after both threads: %d\n",val );
    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */

