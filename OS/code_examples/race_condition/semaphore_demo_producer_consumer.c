/*
 * =====================================================================================
 *
 *       Filename:  simple_threads.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Wednesday 30 August 2017 05:36:16  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */

#include	<stdlib.h>
#include        <stdio.h>
#include        <pthread.h>
#include        <unistd.h>
#include        <semaphore.h>

sem_t sem;
int max = 3;
/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  producer
 *  Description:  
 * =====================================================================================
 */
void* producer ( void* pv )
{
    //entry section of producer is blank
    for ( int i=0; i<max ;i++ )
    {
        //Resource
        printf ( "pthread A instance producer (should be first before consumer )--- \n");
        sleep(1);
        //exit section of producer does up()
        sem_post(&sem);
    }
    pthread_exit(NULL);
}		/* -----  end of function producer  ----- */



/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  consumer
 *  Description:  
 * =====================================================================================
 */
void* consumer ( void* pv )
{
    for ( int i=0; i<max ;i++ )
    {
        //entry section of consumer should wait
        sem_wait(&sem);
        //Resource
        printf ( "pthread B instance (should not be first - wait for producer instance )---  \n");
        sleep(1);
    }
    //exit section of consumer is blank
    pthread_exit(NULL);
}		/* -----  end of function consumer  ----- */


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
int main ( int argc, char *argv[] )
{

    int pshared = 0; // pshared = 0 indicates that semaphore is shared between threads only
    int ival = 0; // initial value of semaphore is set to 0 for producer consumer problems
                    // where we want one thread to ALWAYS execute before another thread

    if (sem_init(&sem, pshared, ival) == -1)
    {
        perror("sem_init failed");
        exit(EXIT_FAILURE);
    }
    pthread_t pt1, pt2;
    pthread_create(&pt2,NULL,consumer,NULL);
    pthread_create(&pt1,NULL,producer,NULL);
    pthread_join(pt1,NULL);
    pthread_join(pt2,NULL);
    sem_destroy(&sem);
    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */
