/*
 * =====================================================================================
 *
 *       Filename:  simple_fork.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Tuesday 29 August 2017 05:48:36  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include	<stdlib.h>
#include	<stdio.h>
#include	<sys/types.h>
#include	<unistd.h>
#include	<sys/wait.h>
#include        <fcntl.h>
#include        <semaphore.h>
#include        <sys/stat.h>


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
    int
main ( int argc, char *argv[] )
{
    int ret;
    int max = 3;
    int status;
    sem_t* ps = sem_open("s1",O_CREAT,0666,1); //Value 1 named semaphore to prevent concurrent execution of parent and child
    sem_t* ps2 = sem_open("s2",O_CREAT,0666,0); //Value zero named semaphore to allow child to ALWAYS execute first

    

    printf ( "Welcome - My pid is %d\n", getpid() );
    printf ( "----------------Starting Fork-------------\n" );
    if ((ret = fork()) == 0)
    {
            sem_wait(ps);
            printf ( "I am child with PID = %d and PPID = %d\n",getpid(), getppid() );
            for ( int i=0; i<max ;i++ )
            {
                printf ( "I am child and i = %d\n",i );
                printf ( "Child going off to sleep\n" );
                sleep(2);
            }
            sem_post(ps);
            sem_post(ps2);

    }
    else if (ret < 0)
    {
            perror ("Fork Error");
            exit(-1);
    }
    else
    {
            sem_wait(ps2);
            sem_wait(ps);
            printf ( "My ret value was %d\n",ret );
            printf ( "I am Original parent with PID = %d and PPID = %d\n",getpid(), getppid() );
            for ( int i=0; i<max ;i++ )
            {
                printf ( "I am parent and i = %d\n",i );
                printf ( "Parent going off to sleep\n" );
                sleep(2);

            }
            sem_post(ps);
            waitpid(-1, &status, WUNTRACED | WCONTINUED);
            //sem_destroy(ps);
            sem_unlink("s1");
            sem_unlink("s2");
            printf ( "Thank you parent and child are complete...\n" );
    }
    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */
