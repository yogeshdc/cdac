/*
 * =====================================================================================
 *
 *       Filename:  simple_fork.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Tuesday 29 August 2017 05:48:36  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include	<stdlib.h>
#include	<stdio.h>
#include	<sys/types.h>
#include	<sys/wait.h>
#include	<unistd.h>

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
    int
main ( int argc, char *argv[] )
{
    int ret;
    int status;
    printf ( "Welcome - My pid is %d\n", getpid() );
    printf ( "----------------Starting Fork-------------\n" );
    if (ret = fork() == 0)
    {
            printf ( "I am child with PID = %d and PPID = %d\n",getpid(), getppid() );
            int k = execl("/bin/ls", "ls","-la",NULL);
            if (k<0)
            {
                perror("execl failed");
            }
            //The code in the child part below this will never execute because execl will overwrite the process space after the call
            printf ( "Child Ends...This should not be printed\n" );
    }
    else if (ret < 0)
    {
            perror ("Fork Error");
            exit(-1);
    }
    else
    {
            printf ( "My ret value was %d\n",ret );
            printf ( "I am Original parent with PID = %d and PPID = %d\n",getpid(), getppid() );
            waitpid(-1,&status,0);
            if WIFEXITED(status)
                printf ( "child exit \n" );

            printf ( "Thank you...\n" );
    }
    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */
