/*
 * =====================================================================================
 *
 *       Filename:  signal_demo.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Friday 01 September 2017 05:26:08  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include	<stdlib.h>
#include	<stdio.h>
#include	<signal.h>



/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  handler_init
 *  Description:  
 * =====================================================================================
 */
void handler_init ( int signo )
{
    printf ( "You can't interrupt me\n" );
}		/* -----  end of function handler_init  ----- */


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  handler_quit
 *  Description:  
 * =====================================================================================
 */
void handler_quit ( int signo )
{
    printf ( "You can't quit me\n" );
}		/* -----  end of function handler_quit  ----- */

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  handler_term
 *  Description:  
 * =====================================================================================
 */
void handler_term ( int signo )
{
    printf ( "You may terminate me, but let me clean-up\n" );
    exit(EXIT_SUCCESS);
}		/* -----  end of function handler_term  ----- */


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  handler_tstp
 *  Description:  
 * =====================================================================================
 */
void handler_tstp ( int signo )
{
    printf ( "You can't suspend me\n" );
}		/* -----  end of function handler_tstp  ----- */


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
    int
main ( int argc, char *argv[] )
{
    signal(SIGINT, handler_init);
    signal(SIGQUIT, handler_quit);
    signal(SIGTERM, handler_term);
    signal(SIGTSTP, handler_tstp);
    while(1)
        pause() //wait for one signal - equivalent to sleep(1); getchar()
    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */



