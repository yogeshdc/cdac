/*
 * =====================================================================================
 *
 *       Filename:  file_mapping_private.c
 *
 *    Description:  :vn
 *
 *        Version:  1.0
 *        Created:  Tuesday 19 September 2017 10:02:46  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include <unistd.h>
#include <sys/mman.h>
#include <string.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>


int main()
{
    struct stat buf;
    void *pbase;
    int fd=open("sample.txt", O_RDWR|O_CREAT);
    if(fd<0)
    {
        perror("open failed");
        exit(1);

    }
    int fs = fstat(fd, &buf);
    if(fs<0)
    {
        perror("fstat failed");
        exit(1);

    }

    int offset=0;
    int maxlen = buf.st_size;
    char output_buf[8192];
    char input_buf[8192] = "ABCDEFGHIKLMNOPQRSTUVWXYZ\0";

    pbase=mmap(NULL,maxlen, PROT_READ|PROT_WRITE, MAP_PRIVATE,fd,offset);
    if(pbase==NULL)
    {
        perror("mmap");
        exit(1);

    }

    // Read contents from mmaped file into output buffer
    strncpy(output_buf, pbase, maxlen);
   
    printf(" %s\n",output_buf);

    //Write contents to mmaped file from input buffer
    //This will copy the contrents to the start of the mmaped file overwriting the old content
    strncpy(pbase, input_buf, strlen(input_buf));
    
    //Write contents to mmaped file from input buffer
    //This will copy the contrents to the iend of the mmaped file 
    strncpy(pbase+maxlen-strlen(input_buf), input_buf, strlen(input_buf));

    // Read contents from mmaped file into output buffer
    strncpy(output_buf, pbase, maxlen);
   
    printf(" %s\n",output_buf);


    munmap(pbase, maxlen);
    close(fd);
}
