/*
 * =====================================================================================
 *
 *       Filename:  my_mmap.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Monday 18 September 2017 12:32:47  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */

#include <unistd.h>
#include <sys/mman.h>
#include <string.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>



int main()
{
    const int page_size=sysconf(_SC_PAGESIZE);
    const int num_pages=10;
    const int max_size=num_pages*page_size;
    void *pbase;
    int fd=-1,offset=0;
    pbase=mmap(NULL,max_size, PROT_READ| PROT_WRITE, MAP_PRIVATE| MAP_ANONYMOUS,fd,offset);
    if(pbase==NULL)
    {
        perror("mmap");
        exit(1);

    }
    char *ptr=pbase;
    for(int i=0;i<num_pages;i++)
    {
        strcpy(ptr,"ABCD\0");
        ptr += page_size;
    }
    ptr=pbase;
    for(int i=0;i<num_pages;i++)
    {
        printf("%lu \t %s\n", ptr, ptr);
        ptr += page_size;
    }
}
