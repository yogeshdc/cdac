/*
 * =====================================================================================
 *
 *       Filename:  test_infinite.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Tuesday 29 August 2017 05:24:08  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include	<stdlib.h>
#include	<stdio.h>
#include	<sys/types.h>
#include	<unistd.h>

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
    int
main ( int argc, char *argv[] )
{
    printf ( "I am infinite process with pid = %d\n",getpid() );
    printf ( "Enter any char to unblock\n" );
    getchar();
    printf ( "Entering infinite loop\n" );
    while(1)
    {
        //getchar();
    }
    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */
