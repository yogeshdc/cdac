/*
 * =====================================================================================
 *
 *       Filename:  simple_threads.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Wednesday 30 August 2017 05:36:16  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */

#include	<stdlib.h>
#include        <stdio.h>
#include        <pthread.h>
#include        <unistd.h>

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  func1
 *  Description:  
 * =====================================================================================
 */
    void*
func1 ( void* pv )
{
    char*ps = pv;
    for ( int i=0; i<3 ;i++ )
    {
        printf ( " %s welcome ----\n",ps );
        printf ( "In %s counter = %d\n",ps,i );
        printf ( "%s - off to sleep.\n",ps );
        sleep(1);
    }
    pthread_exit(NULL);
}		/* -----  end of function func1  ----- */


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
    int
main ( int argc, char *argv[] )
{
    pthread_t pt1, pt2, pt3;
    pthread_create(&pt1,NULL,func1,"ThreadA");
    pthread_create(&pt2,NULL,func1,"ThreadB");
    pthread_create(&pt3,NULL,func1,"ThreadC");
    //sleep(5);
    pthread_join(pt1,NULL);
    pthread_join(pt2,NULL);
    pthread_join(pt3,NULL);
    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */

