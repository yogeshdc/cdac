/*
 * =====================================================================================
 *
 *       Filename:  array_sum_threads.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Thursday 31 August 2017 03:21:37  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */


#include	<stdlib.h>
#include        <stdio.h>
#include        <pthread.h>
#include        <unistd.h>
#include        <sys/syscall.h>

#define NUM_THREADS 8
#define MAX_NUMBERS 10001
#define RANGE_PER_THREAD MAX_NUMBERS/NUM_THREADS
#define REMAINING_THREADS MAX_NUMBERS%NUM_THREADS


int arr[MAX_NUMBERS];

//Global var to store sums of all threads
int thread_total = 0;

void* psum (void*);

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
int main ( int argc, char *argv[] )
{
    int seq_sum = 0;
    int start = 0;
    pthread_t pattr[NUM_THREADS+1];

    //Take inputt
    for (int i=0; i<MAX_NUMBERS; i++)
    {
        arr[i] = rand() % 100;
        seq_sum += arr[i];
    }
    printf ( "Sequential sum is %d\n",seq_sum );

    //Create threads
    for ( int i=0; i<NUM_THREADS; i++ )
    {
        pthread_create (&pattr[i], NULL, psum, (void*) start );
        start +=RANGE_PER_THREAD;
    }

    if (REMAINING_THREADS)
    {
        pthread_create (&pattr[NUM_THREADS], NULL, psum, (void*) (MAX_NUMBERS-REMAINING_THREADS) );
    }

    //Join Threads
    for (int i=0; i<NUM_THREADS; i++)
    {
        pthread_join(pattr[i], NULL);
    }

    //Display sum
    printf ( "Thread calculated total is %d\n", thread_total );

    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */



/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  psum
 *  Description:  
 * =====================================================================================
 */
void* psum ( void* pv )
{
    int start = (int)pv;
    int end = start + RANGE_PER_THREAD;
    if (end>MAX_NUMBERS)
        end = MAX_NUMBERS;
    int sum = 0;

    for ( int i=start; i<end ;i++ )
    {
        sum += arr[i];
    }
    printf("Thread %4d calculated from array index %6d to index %6d subtotal = %6d\n",syscall(SYS_gettid) , start, end, sum);
    thread_total+= sum;
}		/* -----  end of function psum  ----- */
