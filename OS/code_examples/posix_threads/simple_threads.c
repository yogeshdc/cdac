/*
 * =====================================================================================
 *
 *       Filename:  simple_threads.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Wednesday 30 August 2017 05:36:16  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */

#include	<stdlib.h>
#include        <stdio.h>
#include        <pthread.h>
#include        <unistd.h>

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  func1
 *  Description:  
 * =====================================================================================
 */
    void*
func1 ( void* pv )
{
    printf ( "pthread A ---  Welcome\n" );
    for ( int i=0; i<10 ;i++ )
    {
        printf ( "In thread A. counter = %d\n",i );
        sleep(1);
    }
    pthread_exit(NULL);
}		/* -----  end of function func1  ----- */



/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  func2
 *  Description:  
 * =====================================================================================
 */
    void*
func2 ( void* pv )
{
    printf ( "pthread B ---  Welcome\n" );
    for ( int i=0; i<10 ;i++ )
    {
        printf ( "In thread B. Counter = %d\n",i );
        sleep(1);
    }
    pthread_exit(NULL);
}		/* -----  end of function func2  ----- */


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
    int
main ( int argc, char *argv[] )
{
    pthread_t pt1, pt2;
    pthread_create(&pt1,NULL,func1,NULL);
    pthread_create(&pt2,NULL,func2,NULL);
    //sleep(5);
    pthread_join(pt1,NULL);
    pthread_join(pt2,NULL);
    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */

