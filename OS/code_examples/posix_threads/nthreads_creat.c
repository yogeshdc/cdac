/*
 * =====================================================================================
 *
 *       Filename:  nthreads_creat.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Thursday 31 August 2017 02:57:49  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yogesh Chaudhari (), mr.yogesh@gmail.com
 *   Organization:  ACTS CDAC
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
void *efun1(void*);

int main(int args,char* argc[])
{
    int n=10;
    int k=0;
    pthread_t ptarr[n];
    for(int i=0;i<n;i++)
    {
        k=i+1;
        pthread_create(&ptarr[i],NULL,efun1,(void*)k);
    }
    for(int i=0;i<n;i++)
        pthread_join(ptarr[i],NULL);
    return 0;
}

void* efun1(void* pv)
{
    int id=(int)pv;
    printf("%d\n",id);
    
}
