#include<stdio.h>
#include<pthread.h>

void* efun1(void* pv)
{
	int i;
	printf("thread A--welcome\n");
	for(i=1;i<=5;i++)
		printf("A--%d\n",i);
	pthread_exit(NULL);
}
void* efun2(void* pv)
{
	int i;
	printf("thread B--welcome\n");
	for(i=1;i<=5;i++)
		printf("B--%d\n",i);
	pthread_exit(NULL);
}

int main()
{
	pthread_t pt1,pt2;	//thread handles
	pthread_create(&pt1,NULL,efun1,NULL);
	pthread_create(&pt2,NULL,efun2,NULL);
	pthread_join(pt1,NULL);
	pthread_join(pt2,NULL);
	printf("main--thank you\n");
	return 0;
}
