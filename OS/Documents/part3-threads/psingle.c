#include<stdio.h>
#include<pthread.h>

const int max=100;

void* efun(void* pv)
{
	int i;
	char *ps=pv;
	printf("thread %s--welcome\n",ps);
	for(i=1;i<=max;i++)
		printf("%s--%d\n",ps,i);
	pthread_exit(NULL);
}

int main()
{
	pthread_t pt1,pt2,pt3;	//thread handles
	pthread_create(&pt1,NULL,efun,"A1");
	pthread_create(&pt2,NULL,efun,"B2");
	pthread_create(&pt3,NULL,efun,"C3");
	pthread_join(pt1,NULL);
	pthread_join(pt2,NULL);
	pthread_join(pt3,NULL);
	printf("main--thank you\n");
	return 0;
}
