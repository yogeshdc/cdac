#include<stdio.h>
#include<unistd.h>
#include<semaphore.h>
#include<fcntl.h>

int main()
{
	int ret,i,max=10;
	sem_t *ps;
	ps=sem_open("slock",O_CREAT,0666,1);
	printf("Welcome..pid=%d\n",getpid());
	ret=fork();
	if(ret<0) //insufficient resources or process table is full
	{
		perror("fork");
		exit(1);
	}	
	if(ret==0)
	{
		printf("In child..pid=%d,ppid=%d\n",
				getpid(),getppid());		
		sem_wait(ps);
		for(i=1;i<=max;i++)
		{
			printf("child--%d\n",i);
			sleep(1); //usleep
		}
		sem_post(ps);
		exit(0);
	}
	else	//ret>0
	{
		printf("In parent..pid=%d,ppid=%d\n",
				getpid(),getppid());
		sem_wait(ps);
		for(i=1;i<=max;i++)
		{
			printf("parent--%d\n",i);
			sleep(1);
		}
		sem_post(ps);
		//waitpid(-1,&status,0);
		//sem_destroy(ps);
	}
	//printf("Thank you\n");
	return 0;
}
