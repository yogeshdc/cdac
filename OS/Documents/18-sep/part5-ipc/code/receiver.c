#include<stdio.h>
#include<mqueue.h>

int main()
{
	mqd_t mqid;
	mqid=mq_open("/msimple",O_RDONLY);
	if(mqid<0)
	{
		perror("mq_open");
		exit(1);
	}
	char buf[8192];
	int maxlen=8192, prio,nbytes;
	nbytes=mq_receive(mqid,buf,maxlen,prio);
	if(nbytes<0)
	{
		perror("mq_receive");
		exit(2);
	}
	write(1,buf,nbytes);
	mq_close(mqid);
	return 0;
}
