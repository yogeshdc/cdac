#include<stdio.h>
#include<mqueue.h>

int main()
{
	mqd_t mqid;
	mqid=mq_open("/msimple",O_WRONLY|O_CREAT,0666, NULL);
	if(mqid<0)
	{
		perror("mq_open");
		exit(1);
	}
	char str[]="ABCDEFGHIJKLMNOPQRSTUVWXYZ\n";
	int len=strlen(str);
	int prio=5,ret;
	ret=mq_send(mqid,str,len,prio);
	if(ret<0)
	{
		perror("mq_send");
		exit(2);
	}
	mq_close(mqid);
	return 0;
}
