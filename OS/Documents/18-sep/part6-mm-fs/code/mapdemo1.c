#include<unistd.h>
#include<sys/mman.h>

int main()
{
	const int page_size=sysconf(_SC_PAGESIZE);	//4 KB
	const int num_pages=10;	
	const int max_size = num_pages * page_size;
	void *pbase;
	int fd=-1,offset=0;
	pbase=mmap(NULL,max_size, PROT_READ| PROT_WRITE,
		MAP_PRIVATE|MAP_ANONYMOUS, fd, offset);
	if(pbase==NULL)
	{
		perror("mmap");
		exit(1);
	}
	char *ptr=pbase;
	for(i=0;i<num_pages;i++)
	{
		strcpy(ptr,"ABCD");  //*ptr=rand()%100;
		ptr += page_size;	//4096
	}
	//you may also read pages
	munmap(pbase,max_size);
	return 0;
}





