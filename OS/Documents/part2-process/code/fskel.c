#include<stdio.h>
#include<unistd.h>

int main()
{
	int ret;
	printf("Welcome..pid=%d\n",getpid());
	ret=fork();
	if(ret<0)
	{
		perror("fork");
		exit(1);
	}	
	if(ret==0)
	{
		printf("In child..pid=%d,ppid=%d\n",
				getpid(),getppid());		
	}
	else
	{
		printf("In parent..pid=%d,ppid=%d\n",
				getpid(),getppid());
	}
	return 0;
}
