#include<stdio.h>
#include<unistd.h>
#include<sys/wait.h>

int main()
{
	int ret,i,max=5,status;
	printf("Welcome..pid=%d\n",getpid());
	ret=fork();
	if(ret<0) //insufficient resources or process table is full
	{
		perror("fork");
		exit(1);
	}	
	if(ret==0)
	{
		int *ptr=NULL,a=10,b=0,c;
		printf("In child..pid=%d,ppid=%d\n",
				getpid(),getppid());		
		for(i=1;i<=max+5;i++)
		{
			printf("child--%d...pid=%d,ppid=%d\n"
						,i,getpid(),getppid());
			sleep(1); //usleep
		}
		c=a/b;
		*ptr=100;
		exit(5);
		//exit(0);
	}
	else	//ret>0
	{
		printf("In parent..pid=%d,ppid=%d\n",
				getpid(),getppid());
		for(i=1;i<=max;i++)
		{
			printf("parent--%d\n",i);
			sleep(1);
		}
		waitpid(-1,&status,0);
		if(WIFEXITED(status))
		  printf("child exited normally, status:%d\n",WEXITSTATUS(status));
		else
		  printf("child exited abnormally\n");

	}
	//printf("Thank you\n");
	return 0;
}
