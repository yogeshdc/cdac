#include<stdio.h>
#include<unistd.h>

int main()
{
	int ret,i,max=10;
	printf("Welcome..pid=%d\n",getpid());
	ret=fork();
	if(ret<0) //insufficient resources or process table is full
	{
		perror("fork");
		exit(1);
	}	
	if(ret==0)
	{
		printf("In child..pid=%d,ppid=%d\n",
				getpid(),getppid());		
		for(i=1;i<=max;i++)
		{
			printf("child--%d\n",i);
			sleep(1); //usleep
		}
		exit(0);
	}
	else	//ret>0
	{
		printf("In parent..pid=%d,ppid=%d\n",
				getpid(),getppid());
		for(i=1;i<=max;i++)
		{
			printf("parent--%d\n",i);
			sleep(1);
		}
	}
	//printf("Thank you\n");
	return 0;
}
