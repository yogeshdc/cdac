#include<stdio.h>
#include<unistd.h>
#include<sys/wait.h>

int main()
{
	int ret,status,k;
	printf("Welcome..pid=%d\n",getpid());
	ret=fork();
	if(ret<0)
	{
		perror("fork");
		exit(1);
	}	
	if(ret==0)
	{
		printf("In child..pid=%d,ppid=%d\n",getpid(),getppid());		
		//k=execl("/bin/ls","ls",NULL);
		k=execl("/usr/bin/cal","cal","5","2015",NULL);
		if(k<0)
		{
			perror("execl");
			exit(1);
		}
		//printf("child--thank you\n");//redundant if execl succeeds
		//exit(10);
	}
	else
	{
		printf("In parent..pid=%d,ppid=%d\n",getpid(),getppid());
		waitpid(-1,&status,0);
		if(WIFEXITED(status))
			printf("child exit status...%d\n",WEXITSTATUS(status));
		else
			printf("child terminated abnormally\n");
	}
	return 0;
}
