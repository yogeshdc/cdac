#include<stdio.h>
#include<signal.h>

void handler_int(int signo)
{
	printf("you can't interrupt me\n");
}
void handler_quit(int signo)
{
	printf("you can't quit me\n");
}
void handler_term(int signo)
{
	printf("you mayn't terminate,but can\n");
	//exit(0);
}
void handler_tstp(int signo)
{
	printf("you can't suspend me\n");
}
void handler_kill(int signo)
{
	printf("you can kill me\n");
}
int main()
{
	printf("Welcome...pid=%d\n",getpid());
	signal(SIGINT,handler_int);
	signal(SIGQUIT,handler_quit);
	signal(SIGTERM,handler_term);
	signal(SIGTSTP,handler_tstp);
	signal(SIGKILL, handler_kill); //non maskable,no custom handling
	while(1)
		pause();	//sleep(1), getchar()
	return 0;
}
