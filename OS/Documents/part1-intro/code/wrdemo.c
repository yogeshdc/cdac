#include<unistd.h>
#include<stdio.h>
#include<fcntl.h>

int main()
{
	int fd;
	fd=open("simple.txt",O_WRONLY|O_CREAT,0666);
	if(fd<0)
	{
		perror("open");
		exit(1);
	}
	char str[]="Hello Linux!!";
	int nbytes,len=sizeof(str);
	nbytes=write(fd,str,len);
	if(nbytes<0)
	{	
		perror("write");
		exit(2);
	}
	close(fd);
	return 0;
}

