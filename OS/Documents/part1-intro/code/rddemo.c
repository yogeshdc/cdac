#include<unistd.h>
#include<stdio.h>

int main()
{
	int fd;
	fd=open("sample.txt",O_RDONLY);
	if(fd<0)
	{
		perror("open");
		exit(1);
	}
	char buf[128];
	int nbytes,maxlen=128;
	nbytes=read(fd,buf,maxlen);
	if(nbytes<0)
	{	
		perror("read");
		exit(2);
	}
	buf[nbytes]='\0';
	printf("buf=%s\n",buf);
	close(fd);
	return 0;
}

