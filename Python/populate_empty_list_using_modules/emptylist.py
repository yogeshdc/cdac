"""
Empty list and set defined in this module.
It is called by populate_emptylist.py
"""

l1 = []
s1 = set()
