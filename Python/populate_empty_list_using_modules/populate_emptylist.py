"""
Use the empty list and set from emptylist and populate it with numbers 1 through 10

"""


import emptylist
#from emptylist import *

def populate_list():
    for i in range(0, 10):
        emptylist.l1.append(i)
        emptylist.s1.add(i)

def display_list():
    print(emptylist.l1)
    print(emptylist.s1)


populate_list()
display_list()


"""
Output:
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    set([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])


"""
