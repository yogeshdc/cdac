import re
import urllib

url_start = 'http://www.weather-forecast.com/countries/'
country = input("Enter the country whose weather you want to find: ")
my_url = url_start+country

import urllib.request
data = urllib.request.urlopen(my_url).read()
d = data.decode("utf-8")
daytype_start = re.search('<div class="wxicon wx-', d)
while(daytype_start is not None):
    daytype_end = re.search('"></div>', daytype_start.string[daytype_start.end():])
    daytype = daytype_start.string[daytype_start.end():daytype_start.end()+daytype_end.start()]
    d = d[daytype_start.end()+daytype_end.end():]
    daytype_start = re.search('<div class="wxicon wx-', d)
    print(daytype, end='\t')

    temperature_start = re.search("<span class=\"temp\">",d)
    temperature_end = re.search("</span>", d)
    temperature = d[temperature_start.end():temperature_end.start()]
    print (temperature, end='\t')
    city_start = re.search("forecasts/latest\">",d)
    city_end = re.search("</a>", d)
    city = d[city_start.end():city_end.start()]
    print (city)
