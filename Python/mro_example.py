class parent(object):
    def m1(self):
        print("Method of parent")


class gchild(parent):
    def m1(self):
        print("Method of girl child")


class bchild(parent):
    def m1(self):
        print("Method of boy child")


class grandson(bchild, gchild):
    def m1(self):
        print("Method of grandson")



g = grandson()
g.m1()
