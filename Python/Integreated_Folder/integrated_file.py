"""
Adds elements 0 to 20 to list l1 and displays them
"""

l1 = []
for i in range (0,20):
    l1.append(i)

print(l1)

"""
Output:
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19]
"""
