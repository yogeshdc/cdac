"""
Enter 2 numbers and perfrom add/sub/mul/divide ops

"""

a = int(input("Enter first number: "))
b = int(input("Enter second number: "))

add = a+b
print ("Sum of " + str(a) + " and " + str(b) + " = " + str(add))


diff = a-b
print ("Diff of " + str(a) + " and " + str(b) + " = " + str(diff))


div = a//b
print ("Div of " + str(a) + " and " + str(b) + " = " + str(div))


prod = a*b
print ("Prod of " + str(a) + " and " + str(b) + " = " + str(prod))


mod = a%b
print ("Mod of " + str(a) + " and " + str(b) + " = " + str(mod))


"""
Output:
    Enter first number: 3
    Enter first number: 2
    Sum of 3 and 2 = 5
    Diff of 3 and 2 = 1
    Div of 3 and 2 = 1
    Prod of 3 and 2 = 6
    Mod of 3 and 2 = 1


"""
