from itertools import count
class someclass():
    counter = count(1)
    vars = 1
    def __init__(self, a, b):
        self.x= a
        self.y = b
        print("Getting inputs from inside constructor: ")
        self.p = int(input("Enter a number1: "))
        self.q = int(input("Enter a number2: "))
        print("Hello from constructor")
        self.counter = next(self.counter)
        print("There are " + str(self.counter) + " instances of this class")

    def classmethod(self):
        print("Inside method of a class ")
        print("x = " + str(self.x) + " y = " + str(self.y))
        print("p = " + str(self.p) + " q = " + str(self.q))

print ("Getting I/P from main")
x = int(input("Enter a number1: "))
y = int(input("Enter a number2: "))


someobject = someclass(x, y)
someobject1 = someclass(x+10, y+10)
someobject2 = someclass(x+20, y+20)

print(someobject.vars)
print(someobject1.vars)
print(someobject2.vars)

someobject.classmethod()
someobject1.classmethod()
someobject2.classmethod()
