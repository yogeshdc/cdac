class parent:
    def __init__(self):
        print("In parent constructor")

    def parentmethod(self):
        print("In parent1 method")


class parent2:
    def __init__(self):
        print("In parent2 constructor")

    def parentmethod(self):
        print("In parent2 method")


class child(parent, parent2):
    def __init__(self):
        print("In child constructor")

    def childmethod(self):
        print("In child method")
        self.parentmethod(1)
        #This will always access
        #the method from parent1 because parent1 is the
        #first argument in class


p = parent()
c = child()

c.parentmethod(1)
#The below statement is illegal because parent has no access to child methods
#p.childmethod()
