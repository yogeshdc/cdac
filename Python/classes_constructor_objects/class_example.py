class classname:
    class_var_name = 9

    def class_method1(self):
        method_var1 = 1
        method_var2 = 2
        method_sum_var = method_var1 + method_var2
        return method_sum_var


# myobject is an instance of a class
myobject = classname()
print(myobject.class_var_name)

myobject2 = classname()
print(myobject2.class_var_name)

myobject3 = classname()
print(myobject3.class_var_name)

print(myobject.class_method1())
