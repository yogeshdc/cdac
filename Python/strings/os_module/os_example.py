import os

# Getcwd = get the current working directory
pwd = os.getcwd()
print(pwd)

# Make a directory
os.mkdir("Python_Dir_From_OS_Module")
os.mkdir("Python_Dir_From_OS_Module_del")
print ("Created 2 dirs")


# Remove a directory
os.rmdir("Python_Dir_From_OS_Module_del")
print ("Removed one dir")

# Rename a directory
os.rename("Python_Dir_From_OS_Module", "Dir2")

# list the contents of the directory pointed by pwd path
print(os.listdir(pwd))

# change dir
newpath = "../"
print(os.chdir(newpath))
pwd2 = os.getcwd()
print(pwd2)


os.chdir(pwd)
# Make multiple directories
os.makedirs("cdac/iot")
