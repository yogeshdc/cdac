import string

mystr = "some random string"

# Find length of string
print(len(mystr))

# Find index of letter o
print(mystr.index("o"))

# Find number of times letter 'o' occurs in string
print(mystr.count("o"))

# Slice the string
print(mystr[0:5])

# print reverse string using slice
print(mystr[::-1])

# print if string starts with given letter
print(mystr.startswith("S"))

# print if string ends with given letter
print(mystr.endswith("S"))

print(mystr.upper())

print(mystr.lower())

mystr = "india and pakistan"

# Capitalize the first word of the string
print(mystr.capitalize())

# print if string starts with given letter
print(mystr.startswith("I"))

# print if string ends with given letter
print(mystr.endswith("a"))

print(mystr.upper())

print(mystr.lower())

# In capwords every word is capitalized
print(string.capwords("india and pakistan"))

s = "hello india"
print (s.title())
