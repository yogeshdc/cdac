l1 = [0,1,2,3,4,5]
d1 = {0:'0', 1:'1', 2:'2', 3:'3', 4:'4', 5:'5'}
str1 = "012345"
t1 = (0,1,2,3,4,5)

print("list")
for i in l1:
    print(i)

print("dict")
for i in d1:
    print(d1[i])

print("tuple")
for i in t1:
    print(i)


print("string")
for i in str1:
    print(i)

print("file")
fp = open("text", "r")
#line = fp.readline()
#while line:
#    print(line)
#    line = fp.readline()

for i in fp.readlines():
    print(i)
