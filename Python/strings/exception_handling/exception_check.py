a = int(input("enter 1st number: "))
b = int(input("enter 2nd number: "))

try:
    c = a/b
    fp = open("FileNotOnSystem")
    a = strinfwithoutquotes

except ZeroDivisionError as zerr:
    print("Received zero division error {0}".format(zerr))

except FileNotFoundError as fnf:
    print("File not found {0}".format(fnf))

except NameError as ne:
    print("Probably forgot quotes around string {0}".format(ne))

else:
    # This block runs if there are no exceptions in try block
    # continue if normal execution of tryblock
    print("Did not receive any exceptions")


finally:
    # This block ALWAYS executes (irrespective of wheather exception
    # occuring or not
    print("closing fp irrespective of what happened earlier")
    fp.close()
