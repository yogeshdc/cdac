"""
Calls calc to perform mathematical operations add, sub, mul and divide
"""

from calc import add,sub,mul,div

a = int(input("Enter first number"))
b = int(input("Enter second number"))
sub(a,b)
add(a,b)
mul(a,b)
div(a,b)

"""
Output:
    Enter first number2
    Enter second number1
    sub is :1
    sum is :3
    mul is :2
    div is :2


"""
