def my_func(*args):
    print("Func with multiple args")
    for i in args:
        print(i)


my_func(1)
my_func(1, 2)
my_func(1, 2, 3)
my_func(1, 2, 3, 4, 5, [10, 11, 12])
