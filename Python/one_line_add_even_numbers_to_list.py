"""
Program to add even number to a list in one line
"""

list = [i for i in range(0, 100) where i % 2 == 0]
