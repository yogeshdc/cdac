import random

l = [x**2 for x in range(0,100) if x%2 == 0]
print "shuffling in place"
random.shuffle(l)
print l

print "random number from list: "
print random.choice(l)


import time
for i in range(10):
    print "starting sleep"
    time.sleep(1)
    print "Done sleeping"

