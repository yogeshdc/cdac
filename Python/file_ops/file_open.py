"""
Open the file sample.txt and readi/print it contents

"""


fp = open("sample.txt", "r")
print " The file name is: " + fp.name
print " The file is opened in mode: " + fp.mode
print "**************************************"
print "The file contents are: "
a = " "
while a:
    print "at position: " + str(fp.tell())
    a = fp.readline()
    print a
fp.close

"""
Output:


"""

# modes of open:-
# r = read
# w = write
# a = append
# r+ = read + write (does not create new file if not present)
# w+ = read + write (creates a new file if not present)
# a+ = append+read (creates a new file if not present)


"""
Open the file sample2.txt and write some contents into write
Then read/print the file contents

"""


fp = open("sample2.txt", "w")
print "The file name is: " + fp.name
print "the file is opened in mode: " + fp.mode
print "**************************************"
fp.write("Same old same old .... Hello World!!! :) ")
fp.close

fp = open("sample2.txt")
print "Contents after writing are: " + fp.read()
fp.close

"""
Output:


"""


"""
Open the file sample2.txt in append mode and write some contents into write
Then read/print the file contents

"""


fp = open("sample2.txt", "a+")
print "The file name is: " + fp.name
print "the file is opened in mode: " + fp.mode
print "**************************************"
fp.write("This is the all new .... Hello World!!! :) ")
# The seek is required because after write the pointer is at EOF
fp.seek(0, 0)
print "Contents after writing are: " + fp.read()
fp.close

"""
Output:


"""




"""
Sample for truncate and flush()
"""


# Clearing the old message
# Truncate requires append mode
fp = open("sample2.txt", "a+")
print "removing the first 41"
fp.truncate(41)
print "Contents after truncate are: " + fp.read()
# Clearing Entire file
fp.flush()
# flush() does not necessarily write the file's data to disk. Use flush()
# followed by os.fsync() to ensure this behavior.







"""

Additional info
There's typically two levels of buffering involved:
        Internal buffers
        Operating system buffers
            The internal buffers are buffers created by the
            runtime/library/language that you're programming
            against and is meant to speed things up by avoiding
            system calls for every write. Instead, when you write
            to a file object, you write into its buffer, and whenever
            the buffer fills up, the data is written to the actual file
            using system calls.

            However, due to the operating system buffers, this might not mean
            that the data is written to disk. It may just mean that the data is
            copied from the buffers maintained by your runtime into the buffers
            maintained by the operating system.

            If you write something, and it ends up in the buffer (only), and
            the power is cut to your machine, that data is not on disk when the
            machine turns off.

            So, in order to help with that you have the flush and fsync
            methods, on their respective objects.

            The first, flush, will simply write out any data that lingers in a
            program buffer to the actual file. Typically this means that the
            data will be copied from the program buffer to the operating
            system buffer.

            Specifically what this means is that if another process has that
            same file open for reading, it will be able to access the data
            you just flushed to the file. However, it does not necessarily
            mean it has been "permanently" stored on disk.

            To do that, you need to call the os.fsync method which ensures
            all operating system buffers are synchronized with the storage
            devices they're for, in other words, that method will copy data
            from the operating system buffers to the disk.

            Typically you don't need to bother with either method, but if
            you're in a scenario where paranoia about what actually ends up
            on disk is a good thing, you should make both calls as instructed.
"""
import os
os.fsync(fp)
print "Contents after flush are: " + fp.read()
fp.close



"""
Output:


"""
