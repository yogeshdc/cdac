class a(object):
    def __init__(self):
        print("In init of class a")


class b(a):
    def __init__(self):
        super().__init__()
        print("In init of class b")
    pass


b1 = b()
