"""
list
tuple
dictionary


local/global example
"""


list1 = [1, 2, 3]
tuple1 = {4, 5, 6}
dict1 = {7: 'a', 8: 'b', 9: 'c'}


def random_func():
    global list1
    print(list1)
    tuple1 = {10, 11, 12}  # This is local to random_func
    print(tuple1)


def random_dict_func():
    global dict1
    for k in dict1:
        dict1[k]+='a'
        print(dict1[k])


def add(a, b):
    return(a+b)


def sub(a, b):
    return(a-b)


def mul(a, b):
    return(a*b)


def div(a, b):
    return(a/b)


def print_results():
    # We dont need to declare global because
    # we are not modifying dict1 in this func
    print(dict2)


random_func()
random_dict_func()
dict2 = {'add': 0, 'sub': 0, 'mul': 0, 'div': 0}
dict2['add'] = add(1, 1)
dict2['sub'] = sub(1, 1)
dict2['mul'] = mul(1, 1)
dict2['div'] = div(1, 1)
print_results()
